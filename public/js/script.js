(function($) {
    $(document).ready(function() {
        $(window).scroll(function(){
            if ($(window).scrollTop() >= 200) {
                $('.go-to-topppppp').addClass('show');
            }
            else {
                $('.go-to-topppppp').removeClass('show');
            }
        });
        //Skroll to ID Smooth
        $('.smooth').on('click', function(e) {
            e.preventDefault()
            $('html, body').animate(
                {
                    scrollTop: $($(this).attr('href')).offset().top - 60,
                },
                500,
                'linear'
            )
        });
        var win = $(this); //this = window
        if (win.width() >= 992) {
            $('.wrapper').addClass('open_sidebar');
        }else {
            $('.wrapper').removeClass('open_sidebar');
        };
        $('.btn_nav_sm').on('click', function(e) {
            if ($('.wrapper').hasClass('open_sidebar')) {
                $('.wrapper').removeClass('open_sidebar');
            }else {
                $('.wrapper').addClass('open_sidebar');
            }
            e.preventDefault();
        });
        $(window).on('resize', function(){
            var win = $(this); //this = window
            if (win.width() <= 992) {
                // $('.wrapper').addClass('open_sidebar');
                $('.wrapper').removeClass('open_sidebar');
            } if (win.width() >= 1400) {
                $('.wrapper').addClass('open_sidebar');
            }
        });
        $('.btn-forget').on('click',function(e){
            e.preventDefault();
            $('.form-items','.form-content').addClass('hide-it');
            $('.form-sent','.form-content').addClass('show-it');
        })
    });
})(jQuery);

