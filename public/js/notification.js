// notificationjs
let notification = document.getElementById("notification");

// Get the button that opens the modal
let btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
$(".notification-close").click(e => {
    console.log('on');
    notification.style.display = "none";
});

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == notification) {
        notification.style.display = "none";
    }
}
