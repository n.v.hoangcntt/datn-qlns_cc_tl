// const hostname = window.location.hostname;
// const  SITEURL = 'http://localhost/';
$(document).ready(function () {
    //leave

    $('#button_leave_search').click(e => {
        e.preventDefault();
        $('#form_leave_search').submit();
    });

    $("#table_leave #check_all_leave").click(function () {
        if ($("#table_leave #check_all_leave").is(':checked')) {
            $("#table_leave input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
            });

        } else {
            $("#table_leave input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
            });
        }
    });

    $('.action_check_leave').click(function () {
        let check_leave = false;
        $("#table_leave input[type=checkbox]").each(function () {
            if($(this).is(":checked")){
                check_leave = true;
                return false;
            }
        });

        if(check_leave){
           $('.btn_action_leave_check').each(function () {
               $(this).removeClass("disabled");
           });
       }else {
           $('.btn_action_leave_check').each(function () {
               $(this).addClass("disabled");
           });
       }

    });

    $('.btn_action_leave_check').click(function (e) {
        e.preventDefault();
        let url_action = $(this).attr('href');
        $('#form_leave').attr('action', url_action).submit();
    });

    // attendance

    $('#button_attendance_search').click(e => {
        e.preventDefault();
        $('#form_attendance_search').submit();
    });

    $("#table_attendance #check_all_attendance").click(function () {
        if ($("#table_attendance #check_all_attendance").is(':checked')) {
            $("#table_attendance input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
            });

        } else {
            $("#table_attendance input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
            });
        }
    });

    $('.action_check_attendance').click(function () {
        let check_attendance = false;
        $("#table_attendance input[type=checkbox]").each(function () {
            if($(this).is(":checked")){
                check_attendance = true;
                return false;
            }
        });

        if(check_attendance){
            $('.btn_action_attendance_check').each(function () {
                $(this).removeClass("disabled");
            });
        }else {
            $('.btn_action_attendance_check').each(function () {
                $(this).addClass("disabled");
            });
        }

    });

    $('.btn_action_attendance_check').click(function (e) {
        e.preventDefault();
        let url_action = $(this).attr('href');
        $('#form_attendance').attr('action', url_action).submit();
    });

    $('.btn_edit_attendance').click(function (e) {
        let data = $(this).data('data');
        $('#attendance_id').val(data.id);
        $('#text_username').text(data.user.username);
        $('#attendances_date').val(data.attendances_date);

        data.check_in && $('#time_check_in').val(jQuery.format.date(data.check_in, 'H:m'));
        data.check_out && $('#time_check_out').val(jQuery.format.date(data.check_out, 'H:m'));
    });



    jQuery('#time_check_in').datetimepicker({
        datepicker:false,
        step:5,
        format:'H:i'
    });
    $('#time_check_out').datetimepicker({
        datepicker:false,
        step:5,
        format:'H:i'
    });
});


