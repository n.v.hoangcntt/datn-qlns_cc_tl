// const hostname = window.location.hostname;
const SITEURL = 'http://localhost/';
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.btn_add_user').click(function (e) {
        e.preventDefault();
        $('#username').attr("readonly", false);
        let srcDefault = $('#showiamge').data().srcdefault;
        $('#showiamge').attr('src', srcDefault);
        $('#action_user').html('Thêm');
        $('#userForm')[0].reset();
        // document.getElementById('userForm').reset()
        $('#formModalCreateUser').modal('show');
    });

    $('.edit_user').click(function (e) {
        e.preventDefault();
        let data = $(this).data('data');
        let user_id = $(this).data('id');
        fillDataUser(data);
        $('#action_user').html('Sửa');
        $('#formModalCreateUser').modal('show');
    });

    $('#action_user').click(function (e) {
        e.preventDefault();
        let typeAction = $(this).html();
        if (typeAction == 'Thêm') {
            $('#_method').val('post');
            $('#userForm').submit()
        }
        if (typeAction == 'Sửa') {
            let user_id = $('#id').val();
            $('#_method').val('put');
            let url_action = $('#userForm').attr('action');
            $('#userForm').attr('action', url_action + '/' + user_id).submit();
        }

    });

    $('.btn_delete').click(function (e) {
        e.preventDefault();
        let delete_id = $(this).data('id');
        $('#delete_id').val(delete_id);
        $('#confirmModalDeleteUser').modal('show');
    });

    $('#button_confirm_delete_ok').click(e => {
        e.preventDefault();
        let delete_id = $('#delete_id').val();
        let url_action = $('#delete_modal_user').attr('action');
        $('#delete_modal_user').attr('action', url_action + '/' + delete_id).submit();
    });

    $('#button_search').click(e => {
        e.preventDefault();
        $('#form_search').submit();
    });


    //contract
    $('.btn_add_contract').click(function (e) {
        e.preventDefault();
        let user_id = $(this).data('user_id');
        $('#contract_user_id').val(user_id);
        $('#div_file_contract').css("display", "none");
        $('#action_contract').html('Thêm');
        $('#contractForm')[0].reset();
        $('#ThemHopDongModal').modal('show');
    });

    $('.btn_edit_contract').click(function (e) {
        e.preventDefault();
        let data = $(this).data('contract');
        if (data.link) {
            $('#div_file_contract').css("display", "block");
            $('#tai_hop_dong').attr('href', data.link);
            $('#xem_hop_dong').attr('data-link', data.link)
        } else {
            $('#div_file_contract').css("display", "none");
            $('#tai_hop_dong').attr('href', '#');
            $('#xem_hop_dong').attr('data-link', '');
        }
        if (data.accuracy_link) {
            $('#div_file_accuracy').css("display", "block");
            $('#tai_xac_thuc').attr('href', data.accuracy_link);
            $('#xem_xac_thuc').attr('data-link', data.accuracy_link)
        } else {
            $('#div_file_accuracy').css("display", "none");
            $('#tai_xac_thuc').attr('href', '#');
            $('#xem_xac_thuc').attr('data-link', '');
        }
        let user_id = $(this).data('user_id');
        $('#contractForm')[0].reset();
        fillDataContract(data, user_id);
        $('#action_contract').html('Sửa');
        $('#ThemHopDongModal').modal('show');
    });

    $('#action_contract').click(function (e) {
        e.preventDefault();
        let typeAction = $(this).html();
        if (typeAction == 'Thêm') {
            $('#_method').val('post');
            $('#contractForm').submit()
        }
        if (typeAction == 'Sửa') {
            let user_id = $('#id').val();
            $('#_method').val('put');
            let url_action = $('#contractForm').attr('action');
            $('#contractForm').attr('action', url_action + '/' + user_id).submit();
        }

    });
});


const fillDataUser = (data) => {
    $('#username').attr("readonly", "readonly")
    $('#username').val(data.username ? data.username : '');
    $('#id').val(data.id);
    $('#email').val(data.email ? data.email : '');
    $('#password').val(data.password ? data.password : '');
    $('#name').val(data.name ? data.name : '');
    $('#phone_number').val(data.phone_number ? data.phone_number : '');
    $('#bank_account').val(data.bank_account ? data.bank_account : '');
    $('#role_type_id').val(data.role_type_id ? data.role_type_id : 3);
    $('#department_id').val(data.department_id ? data.department_id : '');
    let srcDefault = $('#showiamge').data().srcdefault;
    data.avatar ? $('#showiamge').attr('src', SITEURL + data.avatar) : $('#showiamge').attr('src', srcDefault);
}

const fillDataContract = (data, user_id) => {
    $('#contract_user_id').val(user_id);
    $('#id').val(data.id);
    $('#date_start').val(data.date_start);
    $('#date_end').val(data.date_end);
    $('#insurance_discount').val(data.insurance_discount);
    $('#contract_type_id').val(data.contract_type_id);
    $('#salary').val(data.salary);
}
