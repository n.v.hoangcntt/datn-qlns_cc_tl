$(document).ready(function () {
    const dateNow = new Date();
    $('#xacminh').css('display', 'none');
    $('.btn_edit_leave').click(function(e) {
        // e.preventDefault();
        // e.stopImmediatePropagation ();
        let data = $(this).data('data');
        $('#editLeaveForm')[0].reset();
        $('#leave_type_id_modal').val(data.leave_type_id);
        $('#leave_id').val(data.id);
        $('#content_modal').val(data.content);
        $('#date_leave_modal').val(data.date_leave);
        $('#modalEditLeave').show();
    });


    jQuery(function(){
        jQuery('#date_timepicker_start').datetimepicker({
            format:'Y-m-d',
            onShow:function( ct ){
                this.setOptions({
                    maxDate:jQuery('#date_timepicker_end').val()?jQuery('#date_timepicker_end').val():false,
                    minDate: new Date()
                })
            },
            onChangeDateTime:function(current_time,$input){
                jQuery('#date_timepicker_end').val()?jQuery('#date_timepicker_end').val():jQuery('#date_timepicker_end').val( jQuery('#date_timepicker_start').val())
            },
            timepicker:false
        });
        jQuery('#date_timepicker_end').datetimepicker({
            format:'Y-m-d',
            onShow:function( ct ){
                this.setOptions({
                    minDate:jQuery('#date_timepicker_start').val()?jQuery('#date_timepicker_start').val():false
                })
            },
            onChangeDateTime:function(current_time,$input){
                jQuery('#date_timepicker_start').val()?jQuery('#date_timepicker_end').val():jQuery('#date_timepicker_start').val( jQuery('#date_timepicker_end').val())
            },
            timepicker:false
        });
    });
});

