$(document).ready(function () {
    $.fn.datepicker.defaults.format = "yyyy-mm-dd";
    // $.fn.datepicker.defaults.clearBtn = true;


    // // custom ngôn ngữ
    // $.fn.datepicker.defaults.language = "vi";
    // $.fn.datepicker.dates['vi'] = {
    //     days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    //     daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    //     daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
    //     months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    //     monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    //     today: "Today",
    //     clear: "Clear",
    //     format: "mm/dd/yyyy",
    //     titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    //     weekStart: 0
    // };


    // datetime picker
    // jQuery.datetimepicker.setLocale('vi');

    $('.btn_confirmed_delete').click(function (e) {
        e.preventDefault();
        let redirect = $(this).attr('href');
        $('#btn_modal_confirmed_delete_ok').attr('href', redirect);
        $('#modalConfirmedDelete').modal('show');
    });

//draggable_modal
    if (!($('.modal.in').length)) {
        $('.draggable_modal_dialog').css({
            top: 0,
            left: 0
        });
    }


    $('.draggable_modal_dialog').draggable({
        handle: ".move_modal"
    });

//    view file
    $('.btn_view_file').click(function (e) {
        e.preventDefault();
        let link = $(this).data('link');
        $('#link_file_view').attr('src', link);

        // $('#ThemHopDongModal').modal('hide');
        $('#view_file').modal('show');
    })

});

