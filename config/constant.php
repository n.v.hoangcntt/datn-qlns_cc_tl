<?php
define('URI_DASHBOARD', 'dashboard');
define('URI_SETTING', 'setting');
define('LEAVE_ATTENDANCE', 'leave_attendance');
define('REPORTS', 'reports');
define('URI_USER_MANAGEMENT', 'user_management');
define('DEFAULT_PAGINATION', 10);

define('ROLE',
    [
        "System admin",
        "Quản Lý",
        "Nhân viên"
    ]);
define('CONTRACT_TYPE',
    [
        "Chính Thức",
        "Thử việc",
        "Partime",
    ]
);

define('STATUS_ATTENDANCE',
    [
        "Không tăng ca",
        "Có Tăng ca",
        "Làm ngày nghỉ",
    ]
);

define('LEAVE_TYPE',
    [
        "Nghỉ một ngày",
        "Nghỉ nửa ngày",
        "Nghỉ không công",
        "Nghỉ ôm",
    ]);
