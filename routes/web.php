<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('test', 'HomeController@chartSalary');

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@getLogout')->name('getLogout');


Route::middleware('auth')->group(function () {
//    user

    Route::get('leave', 'LeaveController@index')->name('leave');
    Route::post('leave', 'LeaveController@store')->name('leaveStore');
    Route::post('leave_multi', 'LeaveController@leaveStoreMultiDay')->name('leaveStoreMultiDay');
    Route::get('leave/{id}', 'LeaveController@destroy')->name('leaveDelete');
    Route::put('leave', 'LeaveController@update')->name('leaveUpdate');
    Route::get('manage_leave','LeaveController@manageLeave')->name('manage_leave');
    Route::get('attendance', 'AttendanceController@index')->name('attendance');
    Route::get('manage_attendance','AttendanceController@manageAttendance')->name('manage_attendance');
    Route::get('checkin', 'AttendanceController@checkin')->name('checkin');
    Route::get('checkout', 'AttendanceController@checkout')->name('checkout');


    Route::group(['middleware' => ['checkAdmin']], function () {
//        Import chấm công
        Route::post('import_attendance', 'LeaveAndAttendanceController@importAttendance')->name('import_attendance');
        Route::post('import_day_off', 'DayOffController@importDayOff')->name('import_day_off');

        //    admin
        Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
//    quản lý user
        Route::resource('/user_management', 'UserController')->except([
            'create', 'show', 'edit'
        ]);;
        //quản lý hồ sơ
        Route::resource('/contractFile', 'ContractController')->only([
            'store', 'update'
        ]);;
//        view ngày nghỉ và chấm công
        Route::get('/leave_attendance', 'LeaveAndAttendanceController@index')->name('leave_attendance');
//        report
        Route::get('reports', 'ReportController@index')->name('indexReport');
        Route::get('tinh_luong', 'SalaryController@create')->name('tinhLuong');
        Route::get('/setting', 'SettingController@index')->name('setting');
    });



//    xin nghỉ
    Route::get('yes_leave/{id}', 'LeaveController@yes')->name('yes_leave');
    Route::get('yes_leave_group/{group}', 'LeaveController@yesGroup')->name('yes_leave_group');
    Route::get('no_leave/{id}', 'LeaveController@no')->name('no_leave');
    Route::get('no_leave_group/{group}', 'LeaveController@noGroup')->name('no_leave_group');
    Route::post('yes_multiple_leave', 'LeaveAndAttendanceController@yesMultipleLeave')->name('yes_multiple_leave');
    Route::post('delete_multiple_leave', 'LeaveAndAttendanceController@deleteMultipleLeave')->name('delete_multiple_leave');


//    cham cong attendance
    Route::get('delete_attendance/{id}', 'AttendanceController@destroy')->name('deleteAttendance');
    Route::post('update_attendance', 'AttendanceController@update')->name('update_attendance');
    Route::get('yes_over_time/{id}', 'AttendanceController@yes')->name('yes_over_time');
    Route::get('no_over_time/{id}', 'AttendanceController@no')->name('no_over_time');
    Route::post('yes_multiple_attendance', 'LeaveAndAttendanceController@yesMultipleAttendance')->name('yes_multiple_attendance');
    Route::post('delete_multiple_attendance', 'LeaveAndAttendanceController@deleteMultipleAttendance')->name('delete_multiple_attendance');

//    report
    Route::post('create_report', 'ReportController@create')->name('createReports');
    Route::get('delete_report/{id}', 'ReportController@destroy')->name('delete_report');

//    salary

    Route::get('delete_salary/{id}', 'SalaryController@destroy')->name('delete_salary');
    Route::post('update_salary', 'SalaryController@update')->name('update_salary');

//    setting
    Route::post('/setting', 'SettingController@store')->name('storeSetting');
    Route::get('/timekeeping_off', 'SettingController@timekeepingOff')->name('timekeepingOff');
    Route::get('/timekeeping_on', 'SettingController@timekeepingOn')->name('timekeepingOn');
    Route::post('/update_infor', 'SettingController@updateInfor')->name('updateInfor');
    Route::post('update_tax', 'TaxController@store')->name('updateTax');


    Route::post('create_department', 'DepartmentController@store')->name('createDepartment');
    Route::post('update_department', 'DepartmentController@update')->name('updateDepartment');
    Route::get('delete_department/{id}', 'DepartmentController@destroy')->name('departmentDelete');

//    ngày nghỉ
    Route::get('delete_day_off/{id}', 'DayOffController@destroy')->name('dayOffDelete');
    Route::post('create_day_off', 'DayOffController@store')->name('dayOffCreate');
    Route::post('update_day_off', 'DayOffController@update')->name('dayOffUpdate');

    Route::get('/change_password', 'SettingController@changePassword')->name('changePassword');
    Route::post('/change_password', 'SettingController@storePassword')->name('storePassword');
});
