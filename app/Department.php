<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['id', 'department_name', 'content', 'updated_by', 'head_id'];

    public function user()
    {
        return $this->hasMany('App\User');
    }
}
