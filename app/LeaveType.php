<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveType extends Model
{
    protected $table = 'leave_types';

    protected $fillable = [
        'id', 'leave_name', 'workday',
    ];

    public function leaves()
    {
        return $this->hasMany('App\Leave');
    }
}
