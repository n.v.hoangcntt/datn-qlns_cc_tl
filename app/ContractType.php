<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractType extends Model
{
    //

    public function contract()
    {
        return $this->hasMany('App\Contract');
    }
}
