<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class UsersExport implements FromQuery, WithMapping, WithHeadings, WithEvents, ShouldAutoSize
{
    use Exportable;

    public function __construct($phongban = null)
    {
        $this->phongban = $phongban;
    }


    public function query()
    {
        if($this->phongban){
            $idPhongBan = $this->phongban;
            return User::query()->with(['contract' => function ($q) {
                return $q->with('contractType');
            }, 'department'])->whereHas('department', function ($query) use ($idPhongBan) {
                return $query->where('id', $idPhongBan);
            });
        }else{
            return User::query()->with(['contract' => function ($q) {
                return $q->with('contractType');
            }, 'department']);
        }

    }

    public function map($user): array
    {
        return [
            $user['id'],
            $user->username,
            $user->email,
            $user['contract']['salary'],
            $user['contract']['contract_type']['contract_name'],
            $user['department']['department_name'],
            $user['gender'],
            $user['phone_number'],
            $user['bank_account']
        ];
    }

    public function headings(): array
    {

        return [
            '#ID',
            'Username',
            'Email',
            'Mức lương',
            'Loại hợp đồng',
            'Phòng ban',
            'Giới tính',
            'Số điện thoại',
            'Số tài khoản'
        ];
    }

    public function registerEvents(): array
    {
        $styleArray = [
            'font' => [
                'bold' => true,
            ]
        ];
        return [
            AfterSheet::class => function (AfterSheet $event) use ($styleArray){
            $event->sheet->getStyle('A1:I1')->applyFromArray($styleArray);
            },
        ];
    }
}
