<?php

namespace App\Exports;

use App\Salary;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;

class SalaryExport implements FromQuery, WithMapping, WithHeadings, WithEvents, ShouldAutoSize
{

    private $date;

    public function headings(): array
    {
        return [
            '#ID',
            'Username',
            'Email',
            'Tổng lương',
            'Lương thực lĩnh',
            'Lương tháng',
            'Tiền thưởng',
            'Tiền phụ cấp',
            'Công tăng ca',
            'Tiền phạt',
            'Phí khác',
            'Số ngày công',
            'Số ngày công tăng ca',
            'Tổng số phút đi muộn',
            'Phí Thu nhập cá nhân',
            'Tài khoản ngân hàng'
        ];
    }

    public function query()
    {
        $lastMonth = Carbon::now()->subMonth()->format("Y-m");

        $this->date = $lastMonth;

        return Salary::query()->where('month_of_year', $lastMonth)->with('user');
    }

    public function map($row): array
    {
        return [
            $row['id'],
            $row['user']['username'],
            $row['user']['email'],
            $row['total_salary'],
            $row['result_salary'],
            $row['month_of_year'],
            $row['bonus'],
            $row['allowance'],
            $row['money_over_time'] + $row['money_over_time_free'],
            $row['mulct'],
            $row['other'],
            $row['total_workday'],
            $row['total_over_time'] + $row['total_over_time_free'],
            $row['total_late_time'],
            $row['social_security'],
            $row['user']['bank_account'],
        ];
    }

//    public function columnWidths(): array
//    {
//        return [
//            'A' => 10,
//            'B' => 20,
//            'C' => 50,
//            'D' => 50,
//            'E' => 200,
//        ];
//    }

    public function registerEvents(): array
    {
        $styleArray = [
            'font' => [
                'bold' => true,
            ]
        ];
        return [
            AfterSheet::class => function (AfterSheet $event) use ($styleArray) {
                $event->sheet->getStyle('A1:P1')->applyFromArray($styleArray);
                $event->sheet->setTitle('Lương tháng ' . $this->date);
                $event->sheet->setCellValue('R2', 'Tổng lương thanh toán' . $this->date)->getStyle('R2')
                    ->applyFromArray(
                        ['font' =>
                            ['bold' => true,
                                'size' => 18
                            ],
                        ]
                    );
                $event->sheet->setCellValue('R3', "Tổng số ngày tăng ca")->getStyle('R3')
                    ->applyFromArray(
                        ['font' =>
                            ['bold' => true,
                                'size' => 18
                            ],
                        ]
                    );
                $event->sheet->setCellValue('R4', "Tổng chi tăng ca")->getStyle('R4')
                    ->applyFromArray(
                        ['font' =>
                            ['bold' => true,
                                'size' => 18
                            ],
                        ]
                    );
                $event->sheet->getColumnDimension('R')->setWidth(30);
                /*tổng  cột E có thể dùng =SUBTOTAL(9,A:A)*/
                $event->sheet->setCellValue('S2', '=SUM(E:E)')->getStyle('S2')
                    ->applyFromArray(
                        [
                            'font' =>
                                ['size' => 16],
                        ]
                    );
                $event->sheet->setCellValue('S3', '=SUM(I:I)')->getStyle('S4')
                    ->applyFromArray(
                        [
                            'font' =>
                                ['size' => 16],
                        ]
                    );
                $event->sheet->setCellValue('S4', '=SUM(M:M)')->getStyle('S3')
                    ->applyFromArray(
                        [
                            'font' =>
                                ['size' => 16],
                        ]
                    );
                $event->sheet->getColumnDimension('S')->setWidth(20);
            },
        ];
    }
}
