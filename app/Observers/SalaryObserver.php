<?php

namespace App\Observers;

use App\Salary;
use App\Tax;

class SalaryObserver
{
    /**
     * Handle the salary "created" event.
     *
     * @param \App\Salary $salary
     * @return void
     */
    public function created(Salary $salary)
    {
//        $luongThucLinh = $salary->total_salary - $salary->social_security;
//        if ($luongThucLinh > 0) {
//            $salary->result_salary = $luongThucLinh;
//        } else {
//            $salary->result_salary = 0;
//        }
//
//        $salary->save();
    }

    public function creating(Salary $salary)
    {
        $tienBHMax = Tax::max('taxation_start');
        $mucBaoHiemMax = Tax::where('taxation_start', $tienBHMax)->first();

        $tienCongDiLam = $salary['salary_one_day'] * $salary['total_workday']; // lương một ngày nhân với số ngày đi làm
        $tienLuongNhan = $tienCongDiLam +
            $salary->allowance +
            $salary->bonus +
            $salary->money_over_time +
            $salary->money_over_time_free -
            $salary->other -
            $salary->mulct;
        $thuNhapChiuThue = $tienCongDiLam +
            $salary->allowance +
            $salary->bonus +
            $salary->money_over_time -
            $salary->insurance_discount -
            $salary->other -
            $salary->mulct;
        if ($thuNhapChiuThue > $tienBHMax) {
            $mucBaoHiem = $mucBaoHiemMax;
        } else {
            $mucBaoHiem = Tax::where('taxation_start', '<=', $thuNhapChiuThue)->where('taxation_end', '>=', $thuNhapChiuThue)->orderBy('taxation_start', 'asc')->first();
        }
        $phiTNCN = ($thuNhapChiuThue * $mucBaoHiem->percent / 100) - $mucBaoHiem->tax_down;
//        lưu kết quả
        $phiTNCN > 0 ?  $salary->social_security = (int)$phiTNCN : $salary->social_security = 0;
        $tienLuongNhan > 0 ?  $salary->total_salary = $tienLuongNhan : $salary->total_salary = 0;
        $luongThucLinh = $salary->total_salary - $salary->social_security;
        $luongThucLinh > 0 ?
            $salary->result_salary = $luongThucLinh :
            $salary->result_salary = 0;
    }

    public function updating(Salary $salary)
    {
        $tienBHMax = Tax::max('taxation_start');
        $mucBaoHiemMax = Tax::where('taxation_start', $tienBHMax)->first();

        $tienCongDiLam = $salary['salary_one_day'] * $salary['total_workday']; // lương một ngày nhân với số ngày đi làm
        $tienLuongNhan = $tienCongDiLam +
            $salary->allowance +
            $salary->bonus +
            $salary->money_over_time +
            $salary->money_over_time_free -
            $salary->other -
            $salary->mulct;
        $thuNhapChiuThue = $tienCongDiLam +
            $salary->allowance +
            $salary->bonus +
            $salary->money_over_time -
            $salary->insurance_discount -
            $salary->other -
            $salary->mulct;
        if ($thuNhapChiuThue > $tienBHMax) {
            $mucBaoHiem = $mucBaoHiemMax;
        } else {
            $mucBaoHiem = Tax::where('taxation_start', '<=', $thuNhapChiuThue)->where('taxation_end', '>=', $thuNhapChiuThue)->orderBy('taxation_start', 'asc')->first();
        }
        $phiTNCN = ($thuNhapChiuThue * $mucBaoHiem->percent / 100) - $mucBaoHiem->tax_down;
//        lưu kết quả
        $phiTNCN > 0 ?  $salary->social_security = (int)$phiTNCN : $salary->social_security = 0;
        $tienLuongNhan > 0 ?  $salary->total_salary = $tienLuongNhan : $salary->total_salary = 0;
        $luongThucLinh = $salary->total_salary - $salary->social_security;
        $luongThucLinh > 0 ?
            $salary->result_salary = $luongThucLinh :
            $salary->result_salary = 0;
    }

    /**
     * Handle the salary "updated" event.
     *
     * @param \App\Salary $salary
     * @return void
     */
    public function updated(Salary $salary)
    {
//        $luongThucLinh = $salary->total_salary - $salary->social_security;
//        $luongThucLinh > 0 ?
//            $salary->result_salary = $luongThucLinh :
//            $salary->result_salary = 0;
//        $salary->save();
    }

    /**
     * Handle the salary "deleted" event.
     *
     * @param \App\Salary $salary
     * @return void
     */
    public function deleted(Salary $salary)
    {
        //
    }

    /**
     * Handle the salary "restored" event.
     *
     * @param \App\Salary $salary
     * @return void
     */
    public function restored(Salary $salary)
    {
        //
    }

    /**
     * Handle the salary "force deleted" event.
     *
     * @param \App\Salary $salary
     * @return void
     */
    public function forceDeleted(Salary $salary)
    {
        //
    }
}
