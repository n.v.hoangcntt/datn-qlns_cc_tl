<?php

namespace App\Observers;

use App\Attendance;
use Carbon\Carbon;

class AttendanceObserver
{
    /**
     * Handle the attendance "created" event.
     *
     * @param Attendance $attendance
     * @return void
     */
    public function creating(Attendance $attendance)
    {
//        thời gian vào của ngày hôm đó
        $start_time = Carbon::create(Carbon::create($attendance->attendances_date)->format('Y-m-d') . ' ' . getSetting()->start_time);
//        thời gian ra của hôm đó
        $end_time = Carbon::create(Carbon::create($attendance->attendances_date)->format('Y-m-d') . ' ' . getSetting()->end_time);
// ngày làm bình thường
        if (checkIsNotDayOff($attendance->attendances_date)) {
            $attendance->status = 0;
            $check_in_time = Carbon::create($attendance->check_in);
            $late_time = $start_time->diffInMinutes($check_in_time, false);
            $attendance->late_time = $late_time;

            // tính ngày công nửa ngày
            if ($late_time > 0) {
                $workDay = hourWorkOnDay();
                $late_hour = $start_time->diffInHours($check_in_time);
                $tinh = $late_hour / $workDay;

                if ($tinh <= 0.5) {
                    $attendance->workday = 0.5;
                } else {
                    $attendance->workday = 0;
                }
            } else {
                $attendance->workday = 0.5;
            }

            if ($attendance->check_out) {
                $check_out_time = Carbon::create($attendance->check_out);
                $over_time = 0;
                if($check_in_time->diffInMinutes($end_time, false) >= 0){
                    $over_time = $end_time->diffInMinutes($check_out_time, false);
                }else{
                    $over_time = $check_in_time->diffInMinutes($check_out_time, false);
                }

                if($over_time > 0){
                    $attendance->over_time = (int)$over_time ;
                    $attendance->over_time_push = (int)($over_time * 0.5);
                }else{
                    $attendance->over_time = 0 ;
                    $attendance->over_time_push = 0;
                }


                // tính ngày công của ngày
                if ($late_time > 0) {
                    $workDay = hourWorkOnDay();
                    if ($check_out_time->diffInMinutes($end_time, false) >= 0) {
                        $workHour = $check_in_time->diffInHours($end_time, false);
                    } else {
                        $workHour = $check_in_time->diffInHours($check_out_time);
                    }
                    $forAllDay = $workHour / $workDay;
                    $attendance->workday = lamTronSo($forAllDay);
                } else {
//                    khong di muon
                    $workDay = hourWorkOnDay();
                    if ($check_out_time->diffInMinutes($end_time, false) >= 0) {
                        $workHour = $start_time->diffInHours($check_out_time, false);
                    } else {
                        $workHour = $start_time->diffInHours($end_time, false);
                    }

                    $workHour > 0 ? $workHour = $workHour : $workHour = 0;
                    $forAllDay = $workHour / $workDay;

                    $attendance->workday = lamTronSo($forAllDay);
                }
//                end tinh cong ngafy

            }
        } elseif ($attendance->check_out) {
            $check_in_time = Carbon::create($attendance->check_in);
            $check_out_time = Carbon::create($attendance->check_out);
            $over_time = $check_in_time->diffInMinutes($check_out_time, false);
            $attendance->over_time = (int)($over_time);
            $attendance->over_time_push = (int)($over_time);
            $attendance->status = 2;
        }else{
            $attendance->over_time = 0;
            $attendance->over_time_push = 0;
        }
    }

    /**
     * Handle the attendance "updated" event.
     *
     * @param Attendance $attendance
     * @return void
     */
    public function updating(Attendance $attendance)
    {
//        thời gian vào của ngày hôm đó
        $start_time = Carbon::create(Carbon::create($attendance->attendances_date)->format('Y-m-d') . ' ' . getSetting()->start_time);
//        thời gian ra của hôm đó
        $end_time = Carbon::create(Carbon::create($attendance->attendances_date)->format('Y-m-d') . ' ' . getSetting()->end_time);
// ngày làm bình thường
        if (checkIsNotDayOff($attendance->attendances_date)) {
            $attendance->status = 0;
            $check_in_time = Carbon::create($attendance->check_in);
            $late_time = $start_time->diffInMinutes($check_in_time, false);
            $attendance->late_time = $late_time;

            // tính ngày công nửa ngày
            if ($late_time > 0) {
                $workDay = hourWorkOnDay();
                $late_hour = $start_time->diffInHours($check_in_time);
                $tinh = $late_hour / $workDay;

                if ($tinh <= 0.5) {
                    $attendance->workday = 0.5;
                } else {
                    $attendance->workday = 0;
                }
            } else {
                $attendance->workday = 0.5;
            }

            if ($attendance->check_out) {
                $check_out_time = Carbon::create($attendance->check_out);
                $over_time = 0;
                if($check_in_time->diffInMinutes($end_time, false) >= 0){
                    $over_time = $end_time->diffInMinutes($check_out_time, false);
                }else{
                    $over_time = $check_in_time->diffInMinutes($check_out_time, false);
                }

                if($over_time > 0){
                    $attendance->over_time = (int)$over_time ;
                    $attendance->over_time_push = (int)($over_time * 0.5);
                }else{
                    $attendance->over_time = 0 ;
                    $attendance->over_time_push = 0;
                }


                // tính ngày công của ngày
                if ($late_time > 0) {
                    $workDay = hourWorkOnDay();
                    if ($check_out_time->diffInMinutes($end_time, false) >= 0) {
                        $workHour = $check_in_time->diffInHours($end_time, false);
                    } else {
                        $workHour = $check_in_time->diffInHours($check_out_time);
                    }
                    $forAllDay = $workHour / $workDay;
                    $attendance->workday = lamTronSo($forAllDay);
                } else {
//                    khong di muon
                    $workDay = hourWorkOnDay();
                    if ($check_out_time->diffInMinutes($end_time, false) >= 0) {
                        $workHour = $start_time->diffInHours($check_out_time, false);
                    } else {
                        $workHour = $start_time->diffInHours($end_time, false);
                    }

                    $workHour > 0 ? $workHour = $workHour : $workHour = 0;
                    $forAllDay = $workHour / $workDay;

                    $attendance->workday = lamTronSo($forAllDay);
                }
//                end tinh cong ngafy

            }
        } elseif ($attendance->check_out) {
            $check_in_time = Carbon::create($attendance->check_in);
            $check_out_time = Carbon::create($attendance->check_out);
            $over_time = $check_in_time->diffInMinutes($check_out_time, false);
            $attendance->over_time = (int)($over_time);
            $attendance->over_time_push = (int)($over_time);
            $attendance->status = 2;
        }else{
            $attendance->over_time = 0;
            $attendance->over_time_push = 0;
        }
    }

    /**
     * Handle the attendance "deleted" event.
     *
     * @param Attendance $attendance
     * @return void
     */
    public function deleted(Attendance $attendance)
    {
        //
    }

    /**
     * Handle the attendance "restored" event.
     *
     * @param Attendance $attendance
     * @return void
     */
    public function restored(Attendance $attendance)
    {
        //
    }

    /**
     * Handle the attendance "force deleted" event.
     *
     * @param Attendance $attendance
     * @return void
     */
    public function forceDeleted(Attendance $attendance)
    {
        //
    }
}
