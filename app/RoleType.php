<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleType extends Model
{
    protected $table = 'role_types';

    protected $fillable = [
        'id', 'role_name',
    ];

    public function user()
    {
        return $this->hasMany('App\User', 'role_type_id');
    }

}
