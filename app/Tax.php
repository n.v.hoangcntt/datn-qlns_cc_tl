<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $fillable= ['taxation_start', 'taxation_end', 'percent', 'tax_down'];
}
