<?php

namespace App\Repositories\UserRepository;

use App\Repositories\General\AbstractRepository;
use App\Repositories\General\RepositoryInterface;
use App\User;
use Hash;
use Illuminate\Support\Facades\Auth;

class UserEloquentRepository extends AbstractRepository
{

    function getModel()
    {
        return User::class;
    }

    public function find($id, $columns = ['*'])
    {
        return $this->model->find($id);
    }

    /**
     * @param $params
     * @return mixed
     */
    public function buildSearchQuery($params)
    {
        $query = $this->model->query();
        $query->with(['role', 'contract', 'department']);
        if (isset($params['key_search'])) {

            $query->orWhere('name', 'LIKE', "%{$params['key_search']}%")
                ->orWhere('email', 'LIKE', "%{$params['key_search']}%")
                ->orWhere('role_type_id', array_search($params['key_search'], ROLE) ? ((int)array_search($params['key_search'], ROLE) + 1) : '')
                ->orWhere('phone_number', 'LIKE', "%{$params['key_search']}%");

            $query->orWhereHas('department', function ($query) use ($params) {
                return $query->where('department_name', 'LIKE', "%{$params['key_search']}%");
            });
        }

        return $query;

    }

    public function update(array $input, $id, $attribute = "id")
    {

        if (isset($input['avatar'])) {
            $image = $input['avatar'];
            $avatar = $id . '.' . $image->getClientOriginalExtension();
//            $image->move(public_path('images'), $avatar);
            $image->move(public_path('avatar'), $avatar);
            $urlAvatar = 'avatar/' . $avatar;
        } else {
            $urlAvatar = $this->model->find($id)->image;
        }
        empty($input['password']) ?
            $check = $this->model->where($attribute, $id)->update(
                [
                    'username' => $input['username'],
                    'role_type_id' => $input['role_type_id'],
                    'email' => $input['email'],
                    'name' => $input['name'],
                    'gender' => $input['gender'],
                    'phone_number' => $input['phone_number'],
                    'bank_account' => $input['bank_account'],
                    'department_id' => $input['department_id'],
                    'avatar' => $urlAvatar,
                ]
            )
            :
            $check = $this->model->where($attribute, $id)->update(
                [
                    'username' => $input['username'],
                    'password' => Hash::make($input['password']),
                    'role_type_id' => $input['role_type_id'],
                    'email' => $input['email'],
                    'name' => $input['name'],
                    'gender' => $input['gender'],
                    'phone_number' => $input['phone_number'],
                    'bank_account' => $input['bank_account'],
                    'department_id' => $input['department_id'],
                    'avatar' => $urlAvatar,
                ]
            );
        return $check;
//        return $this->model->find($id);
    }

    public function create(array $input)
    {
        if (isset($input['avatar'])) {
            $image = $input['avatar'];

            $image_name = now()->toDateString() . '_' . rand() . '.' . $image->getClientOriginalExtension();

            $image->move(public_path('avatar'), $image_name);

            $form_data = array(
                'username' => $input['username'],
                'password' => Hash::make($input['password']),
                'role_type_id' => $input['role_type_id'],
                'email' => $input['email'],
                'name' => $input['name'],
                'gender' => $input['gender'],
                'phone_number' => $input['phone_number'],
                'department_id' => $input['department_id'],
                'bank_account' => $input['bank_account'],
                'avatar' => 'avatar/' . $image_name,
            );
        } else {
            $form_data = array(
                'username' => $input['username'],
                'password' => Hash::make($input['password']),
                'role_type_id' => $input['role_type_id'],
                'email' => $input['email'],
                'gender' => $input['gender'],
                'phone_number' => $input['phone_number'],
                'department_id' => $input['department_id'],
                'name' => $input['name'],
                'bank_account' => $input['bank_account'],
            );
        }

        return $this->model->create($form_data);
    }
}
