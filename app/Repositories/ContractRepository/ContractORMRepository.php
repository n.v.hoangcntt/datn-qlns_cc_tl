<?php

namespace App\Repositories\ContractRepository;

use App\Repositories\General\AbstractRepository;
use App\Contract;

class ContractORMRepository extends AbstractRepository
{

    function getModel()
    {
        return Contract::class;
    }

    public function find($id, $columns = ['*'])
    {
        return $this->model->find($id);
    }


    public function update(array $input, $id, $attribute = "id")
    {

        $percent = 100;
        if (isset($input['link'])) {
            $image = $input['link'];
            $avatar = $input['user_id']. '.' . $image->getClientOriginalExtension();
//            $image->move(public_path('images'), $avatar);
            $image->move(public_path('contract'), $avatar);
            $link = 'contract/' . $avatar;
        } else {
            $link = $this->model->find($id)->link;
        }
        if (isset($input['accuracy_link'])) {
            $accuracy = $input['accuracy_link'];
            $accuracy_name =  $input['user_id']. '.' . $accuracy->getClientOriginalExtension();
            $accuracy->move(public_path('accuracy_link'), $accuracy_name);
            $accuracy_link = 'accuracy_link/' . $accuracy_name;
        }else{
            $accuracy_link = $this->model->find($id)->accuracy_link;
        }
        if($input['contract_type_id'] == 2){
            $percent = 80;
        }

        return $this->model->updateOrCreate(
            [ 'user_id' => $input['user_id']],
            [
                'date_start' => $this->checkDate($input['date_start']),
                'date_end' => $this->checkDate($input['date_end']),
                'user_id' => $input['user_id'],
                'salary' => $input['salary'],
                'contract_type_id' => $input['contract_type_id'],
                'insurance_discount' => $input['insurance_discount'],
                'link' => $link,
                'accuracy_link' => $accuracy_link,
                'percent' => $percent
            ]
        );
//        return $this->model->find($id);
    }

    public function create(array $input)
    {
        $percent=100;

        if($input['contract_type_id'] == 2){
            $percent = 80;
        }
        $link = '';
        $accuracy_link = '';
        if (isset($input['link'])) {
            $image = $input['link'];
            $image_name =  $input['user_id']. '.' . $image->getClientOriginalExtension();
            $image->move(public_path('contract'), $image_name);
            $link = 'contract/' . $image_name;
        }
        if (isset($input['accuracy_link'])) {
            $accuracy = $input['accuracy_link'];
            $accuracy_name =  $input['user_id']. '.' . $accuracy->getClientOriginalExtension();
            $accuracy->move(public_path('accuracy_link'), $accuracy_name);
            $accuracy_link = 'accuracy_link/' . $accuracy_name;
        }

        $form_data = array(
            'date_start' => $this->checkDate($input['date_start']),
            'date_end' => $this->checkDate($input['date_end']),
            'user_id' => $input['user_id'],
            'salary' => $input['salary'],
            'contract_type_id' => $input['contract_type_id'],
            'insurance_discount' => $input['insurance_discount'],
            'link' => $link,
            'accuracy_link' => $accuracy_link,
            'percent' => $percent
        );

        return $this->model->updateOrCreate(['user_id' =>$input['user_id']], $form_data);
    }

    public function checkDate($date)
    {
        return empty($date) ? null : date('Y-m-d h:i:s', strtotime($date));
    }
}
