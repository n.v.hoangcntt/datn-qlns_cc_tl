<?php

namespace App\Repositories\General;

use App\Repositories\General\RepositoryInterface;
use Session;

abstract class AbstractRepository implements RepositoryInterface
{

    protected $model;

    public function __construct()
    {
        $this->setModel();
    }

    abstract public function getModel();

    public function setModel()
    {
        return $this->model = app()->make($this->getModel());
    }

    public function all($columns = array('*'))
    {

        return $this->model->get($columns);
    }

    public function search($params = null, $paginate = DEFAULT_PAGINATION)
    {
        if (!is_array($params)) {
            throw new \InvalidArgumentException('Parameter $params must be a valid array.');
        }

        if (isset($params['noPagination']) && (int)$params['noPagination'] === 1) {
            return $this->buildSearchQuery($params)->get();
        }

        return $this->buildSearchQuery($params)->paginate(isset($params['per_page']) && $params['per_page'] ?: $paginate);
    }

    public function buildSearchQuery($params)
    {
        $query = $this->model->query();

        return $query;

    }

    public function findBy($attribute, $value, $columns = array('*'))
    {
        return $this->model->where($attribute, '=', $value)->first($columns);
    }

    public function create(array $input)
    {
        return $this->model->create($input);
    }

    public function update(array $input, $id, $attribute = "id")
    {
        $this->model->where($attribute, "=", $id)->update($input);
        return $this->model->find($id);
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function deleteArray(array $listId)
    {
        return $this->model->destroy($listId);
    }

    public function getAll()
    {
        return $this->model->all();
    }
}
