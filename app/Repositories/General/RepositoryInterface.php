<?php

namespace App\Repositories\General;

interface RepositoryInterface
{
    public function findBy($field, $value, $columns = array('*'));

    public function create(array $input);

    public function update(array $input, $id);

    public function delete($id);
}
