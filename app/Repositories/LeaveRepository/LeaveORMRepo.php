<?php


namespace App\Repositories\LeaveRepository;


use App\Leave;
use App\Repositories\General\AbstractRepository;

class LeaveORMRepo extends AbstractRepository
{
    public function getModel()
    {
       return Leave::class;
    }


    public function search($params = null, $paginate = DEFAULT_PAGINATION )
    {
        if (!is_array($params)) {
            throw new \InvalidArgumentException('Parameter $params must be a valid array.');
        }

        if (isset($params['noPagination']) && (int) $params['noPagination'] === 1) {
            return $this->buildSearchQuery($params)->get();
        }

        return $this->buildSearchQuery($params)->paginate(isset($params['per_page']) && $params['per_page'] ?: $paginate);
    }

    public  function buildSearchQuery($params)
    {

        $query = $this->model->query();

        $query->where('user_id', auth()->user()->id)->with(['leaveType', 'user']);
        if (isset($params['leave_search'])) {
            $query->where('content', 'LIKE', "%{$params['leave_search']}%")
                ->orWhere('date_leave', 'LIKE', "%{$params['leave_search']}%");
        }

        return $query;

    }

}
