<?php

namespace App\Imports;

use App\DayOff;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

//ToCollection, WithHeadingRow
class ImportDayOff implements ToCollection, WithHeadingRow
{

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            DayOff::updateOrCreate(
                [
                    'day_off' => $row['day_off']
                ],
                [
                    'content' => $row['content']
                ]
            );
        }
    }
}
