<?php

namespace App\Imports;

use App\Attendance;
use App\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportAttendance implements ToCollection, WithHeadingRow
{

    public function collection(Collection $rows)
    {
        foreach ($rows as $index => $row) {
            $check = User::where('id', $row['user_id'])->count();
            if ($check) {
                Attendance::updateOrCreate(
                    [
                        'user_id' => $row['user_id'],
                        'attendances_date' => $row['attendances_date']
                    ],
                    [
                        'check_in' => $row['check_in'],
                        'check_out' => $row['check_out']
                    ]);
            }
        }
    }
//cai đặt row bắt đầu
//    public function headingRow(): int
//    {
//        return 2;
//    }
}
