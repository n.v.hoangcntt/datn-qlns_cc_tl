<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Salary extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id',
        'month_of_year',
        'work_month',
        'salary_one_day',
        'insurance_discount',
        'allowance',
        'social_security',
        'bonus',
        'other',
        'total_late_time',
        'average_late_time',
        'total_over_time',
        'total_over_time_free',
        'money_over_time',
        'money_over_time_free',
        'mulct',
        'status',
        'total_workday',
        'total_salary',
        'result_salary'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
