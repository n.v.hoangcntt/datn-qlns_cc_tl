<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DayOff extends Model
{
    protected $fillable = ['day_off', 'content'];
}
