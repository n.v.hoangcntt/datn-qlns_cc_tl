<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingSystem extends Model
{
    protected $fillable = [
        'start_time', 'end_time', 'timekeeping_online' ];

    protected $attributes = [
        'start_time' => '08:00',
        'end_time' => '17:00',
        'timekeeping_online' => 0,
    ];

}
