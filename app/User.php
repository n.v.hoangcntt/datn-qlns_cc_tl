<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'name', 'username', 'email', 'password', 'avatar', 'leave_day', 'department_id', 'phone_number', 'bank_account', 'role_type_id', 'gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo('App\RoleType', 'role_type_id');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function contract()
    {
        return $this->hasOne('App\Contract');
    }

    public function leaves()
    {
        return $this->hasMany('App\Leave');
    }

    public function attendances()
    {
        return $this->hasMany('App\Leave');
    }

    protected static function booted()
    {
        static::addGlobalScope('dontAdmin', function (Builder $builder) {
            $builder->where('role_type_id', '>', 1);
        });
    }

    public function salary()
    {
        return $this->hasMany('App\Salary');
    }
}
