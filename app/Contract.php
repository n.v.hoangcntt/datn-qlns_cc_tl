<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $fillable = [
        'contract_type_id', 'contract_title', 'date_start',
        'date_end', 'content', 'link', 'user_id', 'salary',
        'insurance_discount', 'accuracy_link', 'percent'
    ];

    protected $attributes = [
        'percent' => 100,
    ];
   public function user() {
       return $this->belongsTo('App\User');
   }

   public function contractType() {
       return $this->belongsTo('App\ContractType');
   }

}
