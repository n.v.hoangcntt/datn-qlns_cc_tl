<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attendance extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id', 'attendances_date', 'check_in', 'check_out', 'late_time',
        'over_time', 'over_time_push', 'status', 'workday','updated_by'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
