<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Leave extends Model
{
    protected $table = 'leaves';
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['leave_type_id', 'date_leave', 'content', 'status','user_id', 'accuracy', 'leave_group', 'updated_by'];

    public function leaveType()
    {
        return $this->belongsTo('App\LeaveType');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
