<?php

use App\ContractType;
use App\DayOff;
use App\Department;
use App\Leave;
use App\LeaveType;
use App\RoleType;
use App\SettingSystem;
use App\User;
use Carbon\Carbon;

if (!function_exists('activeRoute')) {
    /**
     * Check active route
     *
     * @param array $uri Array prefix of route
     *
     * @return string
     */
    function activeRoute($uri)
    {
        return request()->route()->uri() == $uri ? 'active' : '';
    }
}

if (!function_exists('getListRole')) {
    /**
     * Check active route
     *
     * @param array $uri Array prefix of route
     *
     * @return string
     */
    function getListRole()
    {
        return RoleType::where('role_name', '!=', 'System admin')->get();
    }
}

if (!function_exists('getListContractType')) {
    /**
     * Check active route
     *
     * @param array $uri Array prefix of route
     *
     * @return string
     *
     */
    function getListContractType()
    {
        return ContractType::all();
    }
}

if (!function_exists('getAllUser')) {

    function getAllUser()
    {
        return User::all();
    }
}

if (!function_exists('getListDepartment')) {

    function getListDepartment()
    {
        return Department::all();
    }
}

if (!function_exists('getSetting')) {

    function getSetting()
    {
        return SettingSystem::find(1);
    }
}

if (!function_exists('getAllDepartment')) {

    function getAllDepartment()
    {
        return Department::all();
    }
}

if (!function_exists('getListLeaveType')) {
    /**
     * Check active route
     *
     * @param array $uri Array prefix of route
     *
     * @return string
     *
     */
    function getListLeaveType()
    {

        return LeaveType::all();
    }
}


if (!function_exists('hourWorkOnDay')) {

    function hourWorkOnDay()
    {
        $setting = SettingSystem::find(1);
        if ($setting) {
            return round(Carbon::parse($setting->start_time)->floatDiffInHours($setting->end_time), 0, PHP_ROUND_HALF_DOWN);
        }
        return 8;
    }
}

if (!function_exists('checkIsNotDayOff')) {
    /**
     * Check active route
     *
     * @param array $uri Array prefix of route
     *
     * @return string
     *
     */
    function checkIsNotDayOff($date)
    {
        return DayOff::whereDate('day_off', $date)->count() == 0;
    }
}
if (!function_exists('dayWorkOfLastDate')) {

    function dayWorkOfLastDate()
    {
        $startLastMonth = Carbon::parse('first day of  last Month ')->format("Y-m-d");
        $endLastMonth = Carbon::parse('last day of  last Month ')->format("Y-m-d");
        $daysOfLastMonth = Carbon::now()->subMonth()->daysInMonth;
        $soNgayLeTrongThang = DayOff::whereBetween('day_off', [$startLastMonth, $endLastMonth])->count();
        return abs($daysOfLastMonth - $soNgayLeTrongThang);
    }
}

if (!function_exists('checkQuanly')) {

    function checkQuanly()
    {
        $head = Department::where('head_id', auth()->user()->id)->first();
        return $head ?? false;
    }
}

if (!function_exists('lamTronSo')) {

    function lamTronSo($number)
    {
        $params = abs($number);
        $floorNumber = floor($params);
        $check = $params - $floorNumber;
        if ($check >= 0 && $check < 0.25) $result = $floorNumber;
        elseif ($check >= 0.25 && $check < 0.75) $result = $floorNumber + 0.5;
        else $result = $floorNumber + 1;

        return $number >= 0 ? $result : -$result;
    }
}
