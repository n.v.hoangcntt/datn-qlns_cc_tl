<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (empty($this->get('id'))) {
            $password = 'required|string|min:4|max:20';
            $email = ['email', 'max:128', 'nullable', 'unique:users,email'];
            $passwordConfirmation = 'required|string|min:4|max:20|same:password';
            $username = 'required|max:20|unique:users,username,';

        } else {
            $password = 'nullable|string|min:4|max:20';
            $email = 'email|max:128|nullable|unique:users,email,'. $this->id;
            $passwordConfirmation = 'nullable|string|min:4|max:20|same:password';
            $username = 'required|max:20';
        }
        return [
            'name' => 'nullable|string|max:50',
            'username' => $username,
            'email' => $email,
            'password' => $password,
            'phone_number' => 'nullable|string|regex:/^(\+[0-9]{2})?0?[0-9]{9}$/',
            'role_type_id' => 'required|integer',
            'password_confirmation' => $passwordConfirmation,
        ];
    }
}
