<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeaveMuitipleDayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->get('leave_type_id') == 4) {
            $accuracy = 'required|file';

        } else {
            $accuracy = '';
        }
        return [
            'accuracy' => $accuracy
        ];
    }

    public function messages()
    {
        return [
            'accuracy.required' => 'Nghỉ ốm cần có hình ảnh xác thực',
        ];
    }
}
