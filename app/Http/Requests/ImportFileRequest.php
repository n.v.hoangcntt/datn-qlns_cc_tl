<?php

namespace App\Http\Requests;

use App\Rules\ExcelRule;
use Illuminate\Foundation\Http\FormRequest;

class ImportFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'import_file' => ['required', new ExcelRule(request()->file('import_file'))],
        ];
    }

    public function messages()
    {
        return [
            'import_file.required' => 'Cần thêm tệp để thực hiện chức năng',
            'import_file.mimes' => 'Tệp tải lên phải thuộc định dạng csv,xlsx,xls',
        ];
    }
}
