<?php

namespace App\Http\Controllers;

use App\DayOff;
use App\Department;
use App\SettingSystem;
use App\Tax;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Session;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        $setting = SettingSystem::find(1);
        $list_tax = Tax::all();
        $list_department = Department::all();
        if (isset($request['day_off_search'])) {
            $day_offs = DayOff::where('day_off', 'LIKE', "%{$request['day_off_search']}%")->orwhere('content', 'LIKE', "%{$request['day_off_search']}%")->orderBy('day_off','DESC')->paginate(DEFAULT_PAGINATION);
        }else {
            $day_offs = DayOff::orderBy('day_off','DESC')->paginate(DEFAULT_PAGINATION);
        }
        return view('admin.setting', compact('setting', 'user', 'list_tax', 'list_department', 'day_offs'));
    }

    public function changePassword()
    {
        $user = auth()->user();
        $setting = SettingSystem::find(1);
        $changePassword = true;
        return view('admin.setting', compact('setting', 'user', 'changePassword'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function timekeepingOff()
    {
        $check = SettingSystem::updateOrCreate(['id' => 1], [
            "timekeeping_online" => 0,
        ]);
        if ($check) {
            Session::flash('success', 'Chấm công online đã Tắt!');
        } else {
            Session::flash('error', 'Có lỗi xảy ra khi cập nhập!');
        }
        return redirect()->route('setting');
    }

    public function timekeepingOn()
    {
        $check = SettingSystem::updateOrCreate(['id' => 1], [
            "timekeeping_online" => 1,
        ]);
        if ($check) {
            Session::flash('success', 'Chấm công online đã Bật!');
        } else {
            Session::flash('error', 'Có lỗi xảy ra khi cập nhập!');
        }
        return redirect()->route('setting');
    }

    public function storePassword(Request $request)
    {
        if (Hash::check($request->oldPassword, auth()->user()->getAuthPassword())) {
            $check = User::withoutGlobalScope('dontAdmin')->find(auth()->id())->update(
                ['password' => Hash::make($request->password)]
            );
            if ($check) {
                Session::flash('success', 'Thay đổi mật khẩu thành công!');
            } else {
                Session::flash('error', 'Có lỗi xảy ra khi cập nhập!');
            }
        } else {
            Session::flash('error', 'Mật khẩu cũ không chính xác!');
        }


        return redirect()->route('changePassword');
    }

    public function updateInfor(Request $request)
    {
            $check = User::withoutGlobalScope('dontAdmin')->find(auth()->id())->update(
              $request->all()
            );
            if ($check) {
                Session::flash('success', 'Cập nhập thông tin thành công!');
            } else {
                Session::flash('error', 'Có lỗi xảy ra khi cập nhập!');
            }
        return redirect()->route('changePassword');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = SettingSystem::updateOrCreate(['id' => 1], [
            "start_time" => $request->start_time,
            "end_time" => $request->end_time
        ]);
        if ($check) {
            Session::flash('success', 'Cập nhập thành công!');
        } else {
            Session::flash('error', 'Có lỗi xảy ra khi cập nhập giờ!');
        }
        return back();
    }

}
