<?php

namespace App\Http\Controllers;

use App\Attendance;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $today = Carbon::now()->format('Y-m-d');

        $checkAttendance = Attendance::where('attendances_date', $today)->where('user_id', auth()->user()->id)->first();
        $attendanceUser = Attendance::where('user_id', auth()->user()->id)->orderBy('created_at', 'DESC')->paginate(DEFAULT_PAGINATION);
        return view('user.attendance', compact('checkAttendance', 'attendanceUser'));
    }

    public function manageAttendance(Request $request)
    {
        $head_id = checkQuanly()['id'];
        if (isset($request['attendance_search'])) {
            $attendances = Attendance::with([
                'user' => function ($query) {
                    $query->withoutGlobalScope('dontAdmin');
                }
            ])->where('user_id', '!=', auth()->user()->id)->whereHas('user', function ($query) use ($request, $head_id) {
                return $query->where('department_id', $head_id)->orWhere('username', 'LIKE', "%{$request['attendance_search']}%");
            })->where(DB::raw("CONVERT(attendances_date, CHAR)"),'like',  "%{$request['attendance_search']}%")->orderBy('attendances_date', 'DESC')->paginate(DEFAULT_PAGINATION, ['*'], 'attendances');
        } else {
            $attendances = Attendance::with([
                'user' => function ($query) {
                    $query->withoutGlobalScope('dontAdmin');
                }
            ])->orWhereHas('user', function ($query) use ($head_id) {
                return $query->where('department_id', $head_id);
            })->where('user_id', '!=', auth()->user()->id)->orderBy('attendances_date', 'DESC')->paginate(DEFAULT_PAGINATION, ['*'], 'attendances');
        }
        return view('user.manage_attendance')->with(['attendances' => $attendances]);
    }

    public function checkin()
    {

        $now = Carbon::now()->format('Y-m-d H:i:s');
        $today = Carbon::now()->format('Y-m-d');
        $user_id = auth()->user()->id;
        $check = Attendance::firstOrCreate(['user_id' => $user_id, 'attendances_date' => $today], ['check_in' => $now]);
        if ($check) {
            Session::flash('success', 'Checkin Thành công!');
        } else {
            Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }

        return redirect()->route('attendance');
    }

    public function checkout() {

        $now  = Carbon::now()->format('Y-m-d H:i:s');
        $today = Carbon::now()->format('Y-m-d');
        $user_id = auth()->user()->id;

        if(Attendance::where('user_id', $user_id)->where('attendances_date', $today)->count()){

            $check = Attendance::updateOrCreate(['user_id'=> $user_id, 'attendances_date' => $today],['check_out' => $now]);
        }else{

            $check = Attendance::updateOrCreate(['user_id'=> $user_id, 'attendances_date' => $today],['check_in' => $now,'check_out' => $now]);
        };

        if ($check) {
            Session::flash('success', 'Checkout Thành công!');
        }else {
            Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }

        return redirect()->route('attendance');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $check = Attendance::updateOrCreate( ['id' => $request->id],['check_in' => "{$request->attendances_date} {$request->time_check_in}" ,'check_out' =>  "{$request->attendances_date} {$request->time_check_out}" ]);
        if ($check) {
            Session::flash('success', 'Cập nhập thành công!');
        }else {
            Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return  back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check = Attendance::destroy($id);
        if ($check) {
            \Session::flash('success', 'Xóa bản ghi thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }

    public function yes($id){
        $data = Attendance::where('id',$id);
        $check = $data->update(['status' => 1]);
        if ($check) {
            \Session::flash('success', 'Xác nhận tăng ca!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }
    public function no($id){
        $data = Attendance::where('id',$id);
        $check = $data->update(['status' => 0]);
        if ($check) {
            \Session::flash('success', 'Hủy bỏ tăng ca!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }
}
