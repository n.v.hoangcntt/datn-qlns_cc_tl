<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\DayOff;
use App\Leave;
use App\Salary;
use App\Tax;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $startLastMonth = Carbon::parse('first day of  last Month ')->format("Y-m-d");
            $endLastMonth = Carbon::parse('last day of  last Month ')->format("Y-m-d");
            $daysOfLastMonth = Carbon::now()->subMonth()->daysInMonth;
            $lastMonth = Carbon::now()->subMonth()->format("Y-m");
            $soNgayLeTrongThang = DayOff::whereBetween('day_off', [$startLastMonth, $endLastMonth])->count();
            $soLamTrongThang = abs($daysOfLastMonth - $soNgayLeTrongThang);
//        $lastMonth = Carbon::now()->subMonth()->format('Y-m');
//        $data = Attendance::where(DB::raw("CONVERT(attendances_date, CHAR)"),'like', '2020-08%')->get();
//        $listAttendance = Attendance::all()->whereBetween('attendances_date', [$startLastMonth,$endLastMonth])->groupBy('user_id');
            $tienBHMax = Tax::max('taxation_start');
            $mucBaoHiemMax = Tax::where('taxation_start', $tienBHMax)->first();
            $gioLamMotNgay = hourWorkOnDay();

            $listUser = User::all();


//        tinh luong tung user
            foreach ($listUser as $user) {
                $sumAttendance = Attendance::whereBetween('attendances_date', [$startLastMonth, $endLastMonth])->where('user_id', $user->id)->sum('workday');
                $soNgayCongNghiPhep = Leave::whereBetween('date_leave', [$startLastMonth, $endLastMonth])->where('user_id', $user->id)->where('status', '>', 0)
                    ->with(['leaveType'])->get()->sum('leaveType.workday');
                $luongNet = $user->contract['salary'] ?? 0;
                $luongMotNgay = $luongNet / $soLamTrongThang;

                $phutTangCa = Attendance::whereBetween('attendances_date', [$startLastMonth, $endLastMonth])
                    ->where('user_id', $user->id)
                    ->where('status', '>', 0)->sum('over_time');

                $soPhutDiMuon = Attendance::whereBetween('attendances_date', [$startLastMonth, $endLastMonth])
                    ->where('user_id', $user->id)->sum('late_time');

                $soNgayTangCa = round(($phutTangCa / 60) / $gioLamMotNgay, 0, PHP_ROUND_HALF_EVEN);

                $tienLuong = round($luongMotNgay * ($sumAttendance + $soNgayTangCa));

                if ($tienLuong > $tienBHMax) {
                    $mucBaoHiem = $mucBaoHiemMax;
                } else {
                    $mucBaoHiem = Tax::where('taxation_start', '<=', $tienLuong)->where('taxation_end', '>=', $tienLuong)->orderBy('taxation_start', 'asc')->first();
                }
                $social_security = ($tienLuong * $mucBaoHiem->percent / 100) - $mucBaoHiem->tax_down;
                $tongNgaycong = $sumAttendance + $soNgayCongNghiPhep;
                Salary::updateOrCreate(['user_id' => $user->id, 'month_of_year' => $lastMonth],
                    [
                        'social_security' => $social_security,
                        'total_late_time' => $soPhutDiMuon,
                        'total_over_time' => $phutTangCa,
                        'total_workday' => $tongNgaycong,
                        'total_salary' => $tienLuong,
                        'result_salary' => (int)($tienLuong - $social_security),
                    ]);

            }

            Session::flash('success', 'Tính lương thành công!');
            return back();
        } catch (\Exception $exception) {

            logger(__METHOD__ . __LINE__ . $exception->getMessage());
            Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
//            return back()->withError($exception->getMessage())->withInput();
            return back();
        }
    }

    public function store(Request $request)
    {
        //
    }

    public function update(Request $request)
    {
        $check = Salary::updateOrCreate(
            ['user_id' => $request->user_id, 'month_of_year' => $request->month_of_year],
            $request->all());
        if ($check) {
            \Session::flash('success', 'Cập nhập thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check = Salary::destroy($id);
        if ($check) {
            \Session::flash('success', 'Xóa thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }
}
