<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUpdateUserRequest;
use App\Repositories\UserRepository\UserEloquentRepository;
use App\RoleType;
use App\User;
use Session;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $user;

    public function __construct(UserEloquentRepository $userRepository)
    {
        $this->user = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $listUser = $this->user->search($request->all());

        return view('admin.user_management',compact('listUser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateUserRequest $request)
    {
        $check =  $this->user->create($request->all());
        if ($check) {
            Session::flash('success', 'Tạo  thành công user!');
        }else {
            Session::flash('error', 'Có lỗi xảy ra khi tạo user vui lòng thử lại!');
        }
        return redirect(route('user_management.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateUserRequest $request, $id)
    {
        $check = $this->user->update($request->all(), $id);
        if ($check) {
            Session::flash('success', 'Cập nhập thành công thông tin!');
        }else {
            Session::flash('error', 'Có lỗi xảy ra khi cập nhập vui lòng thử lại!');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check = $this->user->delete($id);
        if ($check) {
            Session::flash('success', 'Xóa thành công user');
        }else {
            Session::flash('error', 'Có lỗi xảy ra khi xóa user');
        }
        return redirect()->back();
    }
}
