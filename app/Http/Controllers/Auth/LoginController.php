<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    private const redirectUser = 'home';
    private const redirectAdmin = 'dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout','getLogout']);
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function username()
    {
        return 'username';
    }

    public function login(LoginRequest $request)
    {
        if (Auth::attempt(['email' => $request->username, 'password' => $request->password], $request->remember) || Auth::attempt(['username' => $request->username, 'password' => $request->password], $request->remember)) {
           return $this->checkRoleTypeUserRedirect();
        }

        if ((User::where('username', $request->username)->orWhere('email', $request->username)->first())) {
            throw ValidationException::withMessages([
                'password' => __('auth.Incorrect password!'),
            ]);

        } else {
            throw ValidationException::withMessages([
                'username' => [trans('auth.failed')],
                'authFailed' => trans('auth.Invalid Login or password')
            ]);
        }
    }

    public static function checkRoleTypeUserRedirect()
    {
        if (Auth::check()) {
            if ((int)Auth::user()->role_type_id <= 2) {
                return redirect( self::redirectAdmin);
            } else {
                return redirect(self::redirectUser);
            }
        }
    }
}
