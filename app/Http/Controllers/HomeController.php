<?php

namespace App\Http\Controllers;

use App\Salary;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function chartSalary(){
       $data = Salary::where('user_id', auth()->user()->id)->orderBy('month_of_year')->select('month_of_year as date', 'result_salary as value')->get();
       $check = count($data->toArray());
       return $check ? json_encode($data->toArray()) : '';
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('user.home');
    }

    public function dashboard()
    {
        $countUser = User::all()->count();
        return view('admin.dashboard')->with('countUser', $countUser);
    }
}
