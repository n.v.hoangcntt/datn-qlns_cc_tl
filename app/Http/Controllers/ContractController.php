<?php

namespace App\Http\Controllers;


use App\Repositories\ContractRepository\ContractORMRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class ContractController extends Controller
{
    private $repo;

    public function __construct(ContractORMRepository $contractRepository)
    {
        $this->repo = $contractRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//       return  $this->repo->getModel()::where('id', 1)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $check =  $this->repo->create($request->all());
        if ($check) {
            Session::flash('success', 'Thêm hợp đồng thành công!');
        }else {
            Session::flash('error', 'Có lỗi xảy ra khi  thêm hợp đồng vui lòng thử lại!');
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check =  $this->repo->update($request->all(), $id);
        if ($check) {
            Session::flash('success', 'Thêm hợp đồng thành công!');
        }else {
            Session::flash('error', 'Có lỗi xảy ra khi  thêm hợp đồng vui lòng thử lại!');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
