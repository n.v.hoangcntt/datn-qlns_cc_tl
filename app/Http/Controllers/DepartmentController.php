<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = Arr::add($request->all(), 'updated_by', auth()->user()->id);
        $check = Department::updateOrCreate(['department_name' => $request->department_name],
            $params);
        if ($check) {
            \Session::flash('success', 'Thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $check = Department::updateOrCreate(['id' => $request->id],
            $request->all());
        if ($check) {
            \Session::flash('success', 'Thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check = Department::destroy($id);
        if ($check) {
            \Session::flash('success', 'Xóa thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }
}
