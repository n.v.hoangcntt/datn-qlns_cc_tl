<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeaveMuitipleDayRequest;
use App\Leave;
use Session;
use Illuminate\Http\Request;
use App\Repositories\LeaveRepository\LeaveORMRepo;


class LeaveController extends Controller
{
    private $repo;

    public function __construct(LeaveORMRepo $leaveRepository)
    {
        $this->repo = $leaveRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $leaves = $this->repo->search($request->all());
        return view('user.leave')->with(['leaves' => $leaves]);
    }

    public function manageLeave(Request $request)
    {
        $head_id = checkQuanly()['id'];
        if (isset($request['leave_search'])) {
            $leaves = Leave::with([
                'user' => function ($query) {
                    $query->withoutGlobalScope('dontAdmin');
                }
            ])->where('user_id','!=', auth()->user()->id )->orWhere('content', 'LIKE', "%{$request['leave_search']}%")
                ->orWhere('date_leave', 'LIKE', "%{$request['leave_search']}%")
                ->orWhere('leave_group', 'LIKE', "%{$request['leave_search']}%")
                ->orWhereHas('user', function ($query) use ($request, $head_id) {
                    return $query->where('department_id', $head_id)->where('username', 'LIKE', "%{$request['leave_search']}%")
                        ->orWhere('email','LIKE', "%{$request['leave_search']}%");
                })->orderBy('created_at','DESC')->orderBy('date_leave', 'ASC')->paginate(DEFAULT_PAGINATION, ['*'], 'leaves');
        }else {
            $leaves = Leave::with([
                'user' => function ($query) {
                    $query->withoutGlobalScope('dontAdmin');
                }
            ])->orWhereHas('user', function ($query) use ($head_id) {
                return $query->where('department_id', $head_id);
            })->where('user_id','!=', auth()->user()->id )->orderBy('created_at','DESC')->orderBy('date_leave', 'ASC')->paginate(DEFAULT_PAGINATION, ['*'], 'leaves');
        }
        return view('user.manage_leave')->with(['leaves' => $leaves]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Leave::where('user_id', auth()->user()->id)->where('date_leave', $request->date_leave)->count()) {
            Session::flash('error', 'Ngày xin nghỉ đã tồn tại');
        } elseif (!checkIsNotDayOff($request->date_leave)) {
            Session::flash('error', 'Ngày xin phép vào ngày nghỉ');
        } else {
            $check = Leave::create(
                [
                    'user_id' => auth()->user()->id,
                    'date_leave' => $request->date_leave,
                    'leave_type_id' => $request->leave_type_id,
                    'content' => $request['content']
                ]
            );
            if ($check) {
                Session::flash('success', 'Tạo đơn xin nghỉ thành công!');
            } else {
                Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
            }
        }
        return back();
    }

    public function leaveStoreMultiDay(LeaveMuitipleDayRequest $request)
    {
        try {
            $start_date = \Carbon\Carbon::parse($request->date_leave_start);
            $end_date = \Carbon\Carbon::parse($request->date_leave_end);
            $accuracy = null;

            if ($request->leave_type_id == 4) {
                $accuracy = '';
                if ($request->hasFile('accuracy')) {
                    $image = $request['accuracy'];
                    $image_name = auth()->user()->id . $request->date_leave_start . rand() . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('accuracy'), $image_name);
                    $accuracy = 'accuracy/' . $image_name;
                }
            }

            $leave_group = auth()->user()->id . $request->date_leave_start . rand();

            if ($start_date->eq($end_date)) {
                if (Leave::where('user_id', auth()->user()->id)->where('date_leave', $request->date_leave_start)->count()) {
                    Session::flash('error', 'Ngày xin nghỉ đã tồn tại');
                } elseif (!checkIsNotDayOff($request->date_leave_start)) {
                    Session::flash('error', 'Ngày xin phép vào ngày nghỉ');
                } else {
                    if ($request->leave_type_id == 4) {

                        $check = Leave::create(
                            [
                                'user_id' => auth()->user()->id,
                                'date_leave' => $request->date_leave_start,
                                'leave_type_id' => $request->leave_type_id,
                                'content' => $request['content'],
                                'accuracy' => $accuracy
                            ]
                        );
                    } else {
                        $check = Leave::create(
                            [
                                'user_id' => auth()->user()->id,
                                'date_leave' => $request->date_leave_start,
                                'leave_type_id' => $request->leave_type_id,
                                'content' => $request['content']
                            ]
                        );
                    }

                    if ($check) {
                        Session::flash('success', 'Tạo đơn xin nghỉ thành công!');
                    } else {
                        Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
                    }
                }
                return back();
            }

            while (!$start_date->gt($end_date)) {

                if (checkIsNotDayOff($start_date)) {
                    Leave::updateOrCreate(
                        [
                            'user_id' => auth()->user()->id,
                            'date_leave' => $start_date->format('Y-m-d'),
                        ],
                        [
                            'user_id' => auth()->user()->id,
                            'date_leave' => $start_date->format('Y-m-d'),
                            'leave_type_id' => $request->leave_type_id,
                            'content' => $request['content'],
                            'accuracy' => $accuracy,
                            'leave_group' => $leave_group
                        ]
                    );
                };
                $start_date->addDay();
            }

            Session::flash('success', 'Tạo đơn xin nghỉ thành công!');
            return back();
        } catch (Throwable $e) {
            report($e);
            Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại sau ít phút!');
            return false;
        }

    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (Leave::where('user_id', auth()->user()->id)->where('id', $request->id)->count()) {
            $check = Leave::find($request->id)->update(
                [
                    'date_leave' => $request->date_leave,
                    'leave_type_id' => $request->leave_type_id,
                    'content' => $request['content']
                ]
            );
            if ($check) {
                Session::flash('success', 'Cập nhập thành công!');
            } else {
                Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
            }

        } else {
            Session::flash('error', 'Bạn không có quyền');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (Leave::where("id", $id)->where("user_id", auth()->user()->id)->get()->count() || (int)auth()->user()->role_type_id <= 2) {
            $check = Leave::destroy($id);
            if ($check) {
                Session::flash('success', 'Xóa đơn nghỉ thành công!');
            } else {
                Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
            }
        } else {
            Session::flash('error', 'Bản ghi đã được xóa hoặc bạn không có quyền!');
        }

        return back();

    }

    public function yes($id)
    {
        $leave = Leave::find($id);
        $leave->status = 1;
        $leave->updated_by = auth()->user()->id;
        $check = $leave->save();
        if ($check) {
            \Session::flash('success', 'Duyệt thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }

    public function yesGroup($group)
    {
        $check = Leave::where('leave_group', $group)->update(['status' => 1, 'updated_by' => auth()->user()->id]);
        if ($check) {
            \Session::flash('success', 'Duyệt nhóm' . $group . ' thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }

    public function no($id)
    {
        $leave = Leave::find($id);
        $leave->status = 0;
        $leave->updated_by = auth()->user()->id;
        $check = $leave->save();
        if ($check) {
            \Session::flash('success', 'Hủy thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }

    public function noGroup($group)
    {
        $check = Leave::where('leave_group', $group)->update(['status' => 0, 'updated_by' => auth()->user()->id]);;
        if ($check) {
            \Session::flash('success', 'Hủy  nhóm ' . $group . ' thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }
}
