<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Http\Requests\ImportFileRequest;
use App\Imports\ImportAttendance;
use App\Leave;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\Session;

class LeaveAndAttendanceController extends Controller
{
    public function index(Request $request)
    {
        if (isset($request['leave_search'])) {
            $leaves = Leave::with([
                'user' => function ($query) {
                    $query->withoutGlobalScope('dontAdmin');
                }
            ])->where('content', 'LIKE', "%{$request['leave_search']}%")
                ->orWhere('date_leave', 'LIKE', "%{$request['leave_search']}%")
                ->orWhere('leave_group', 'LIKE', "%{$request['leave_search']}%")
                ->orWhereHas('user', function ($query) use ($request) {
                return $query->where('username', 'LIKE', "%{$request['leave_search']}%")
                    ->orWhere('email','LIKE', "%{$request['leave_search']}%");
            })->orderBy('created_at','DESC')->orderBy('date_leave', 'ASC')->paginate(DEFAULT_PAGINATION, ['*'], 'leaves');
        }else {
            $leaves = Leave::with([
                'user' => function ($query) {
                    $query->withoutGlobalScope('dontAdmin');
                }
            ])->orderBy('created_at','DESC')->orderBy('date_leave', 'ASC')->paginate(DEFAULT_PAGINATION, ['*'], 'leaves');
        }


        if (isset($request['attendance_search'])) {
            $attendances = Attendance::with([
                'user' => function ($query) {
                    $query->withoutGlobalScope('dontAdmin');
                }
            ])->whereHas('user', function ($query) use ($request) {
                return $query->where('username', 'LIKE', "%{$request['attendance_search']}%");
            })->orderBy('attendances_date','DESC')->paginate(DEFAULT_PAGINATION, ['*'], 'attendances');
        }else {
            $attendances = Attendance::with([
                'user' => function ($query) {
                    $query->withoutGlobalScope('dontAdmin');
                }
            ])->orderBy('attendances_date','DESC')->paginate(DEFAULT_PAGINATION, ['*'], 'attendances');
        }

        return view('admin.leave_attendance', compact('leaves', 'attendances'));
    }

    public function yesMultipleLeave(Request $request){
        $check = Leave::whereIn('id', $request->id_leave)->update(['status' => 1, 'updated_by' => auth()->user()->id]);
        if ($check) {
            \Session::flash('success', 'Duyệt  thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }

    public function deleteMultipleLeave(Request $request){
        Leave::whereIn('id', $request->id_leave)->update(['updated_by' => auth()->user()->id]);
        $check =  Leave::destroy($request->id_leave);
        if ($check) {
            \Session::flash('success', 'Xóa  thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }

    public function yesMultipleAttendance(Request $request){
        $check = Attendance::whereIn('id', $request->id_attendance)->update(['status' => 1, 'updated_by' => auth()->user()->id]);
        if ($check) {
            \Session::flash('success', 'Xác nhận tăng ca!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }


    public function deleteMultipleAttendance(Request $request)
    {
        Attendance::whereIn('id', $request->id_attendance)->update(['updated_by' => auth()->user()->id]);
        $check = Attendance::destroy($request->id_attendance);
        if ($check) {
            \Session::flash('success', 'Xóa bản ghi thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }

    public function importAttendance(ImportFileRequest $request)
    {
        try {
            Excel::import(new ImportAttendance(), request()->file('import_file'));
            Session::flash('success', 'Import tệp thành công!');
            return back();
        } catch (\Exception $exception) {

            logger(__METHOD__ . __LINE__ . $exception->getMessage());
            Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
//            return back()->withError($exception->getMessage())->withInput();
            return back();
        }
    }
}
