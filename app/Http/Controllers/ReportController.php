<?php

namespace App\Http\Controllers;

use App\Exports\SalaryExport;
use App\Exports\UsersExport;
use App\Report;
use App\Salary;
use App\User;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\Session;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (isset($request['key_search_salary'])) {
            $salary = Salary::with([
                'user' => function ($query) {
                    $query->withoutGlobalScope('dontAdmin');
                }
            ])->whereHas('user', function ($query) use ($request) {
                return $query->where('username', 'LIKE', "%{$request['key_search_salary']}%");
            })->orWhere('month_of_year', 'LIKE', "%{$request['key_search_salary']}%")
                ->orderBy('updated_at', 'DESC')
                ->paginate(DEFAULT_PAGINATION, ['*'], 'salary');
        } else {
            $salary = Salary::with([
                'user' => function ($query) {
                    $query->withoutGlobalScope('dontAdmin');
                }
            ])->orderBy('updated_at', 'DESC')
                ->paginate(DEFAULT_PAGINATION, ['*'], 'salary');
        }

        if (isset($request['key_search_report'])) {
            $reports = Report::where('content', 'LIKE', "%{$request['key_search_report']}%")
                ->orderBy('updated_at', 'DESC')->paginate(DEFAULT_PAGINATION, ['*'], 'reports');
        } else {
            $reports = Report::orderBy('updated_at', 'DESC')->paginate(DEFAULT_PAGINATION, ['*'], 'reports');
        }

        return view('admin.report', compact('salary', 'reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        try {
            $file_name = now()->toDateString() . '-' . rand() . $request['content'];
            if ($request->type_report == 'user') {
                $check = Excel::store(new UsersExport($request['phongban']), $file_name . '.xlsx', 'report_public');
                $check1 = Excel::store(new UsersExport($request['phongban']), $file_name . '.html', 'report_pdf');
            }elseif ($request->type_report == 'luongThangTruoc'){
                $check = Excel::store(new SalaryExport(), $file_name . '.xlsx', 'report_public');
                $check1 = Excel::store(new SalaryExport(), $file_name . '.html', 'report_pdf', \Maatwebsite\Excel\Excel::HTML);
            }
            if($check){
                Report::create(
                    [
                        'content' => $request['content'],
                        'link' => 'report/' . $file_name . '.xlsx',
                        'link_pdf' => 'reportpdf/' . $file_name . '.html'
                    ]);
                Session::flash('success', 'Tạo báo cáo thành công!');
            }else{
                Session::flash('error', 'Tạo file thất bại!');
            }

            return back();
        } catch (\Exception $exception) {

            logger(__METHOD__ . __LINE__ . $exception->getMessage());
            Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
//            return back()->withError($exception->getMessage())->withInput();
            return back();
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check = Report::destroy($id);
        if ($check) {
            \Session::flash('success', 'Xóa thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }
}
