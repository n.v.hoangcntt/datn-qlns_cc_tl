<?php

namespace App\Http\Controllers;

use App\DayOff;
use App\Http\Requests\ImportFileRequest;
use App\Imports\ImportDayOff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Excel;

class DayOffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = DayOff::updateOrCreate(['day_off' => $request->day_off], $request->all());
        if ($check) {
            \Session::flash('success', 'thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DayOff  $dayOff
     * @return \Illuminate\Http\Response
     */
    public function show(DayOff $dayOff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DayOff  $dayOff
     * @return \Illuminate\Http\Response
     */
    public function edit(DayOff $dayOff)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DayOff  $dayOff
     * @return \Illuminate\Http\Responseu
     */
    public function update(Request $request)
    {
        $check = DayOff::updateOrCreate(['id' => $request->id], $request->all());
        if ($check) {
            \Session::flash('success', 'Thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DayOff  $dayOff
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $check = DayOff::destroy($id);
        if ($check) {
            \Session::flash('success', 'Xóa thành công!');
        } else {
            \Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
        }
        return back();
    }

    public function importDayOff(ImportFileRequest $request)
    {
        try {
            Excel::import(new ImportDayOff(), request()->file('import_file'));
            Session::flash('success', 'Import ngày nghỉ thành công!');
            return back();
        } catch (\Exception $exception) {

            logger(__METHOD__ . __LINE__ . $exception->getMessage());
            Session::flash('error', 'Có lỗi xảy ra vui lòng thử lại!');
//            return back()->withError($exception->getMessage())->withInput();
            return back();
        }
    }
}
