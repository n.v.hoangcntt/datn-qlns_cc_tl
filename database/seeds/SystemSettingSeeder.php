<?php

use Illuminate\Database\Seeder;

class SystemSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('setting_systems')->updateOrInsert(
            ['id' => 1],
            [
                'start_time' => '08:00',
                'end_time' => '17:00',
                'timekeeping_online' => 0,
            ]);
    }
}
