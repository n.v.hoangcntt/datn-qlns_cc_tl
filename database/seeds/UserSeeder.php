<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected $listUser = [
        ['username' => 'admin', 'email' => 'itmrhoang@gmail.com', 'password' => 'admin1234', 'role_type_id' => '1'],
        ['username' => 'nvhoang', 'email' => 'n.v.hoangcntt@gmail.com', 'password' => 'hoang2510', 'role_type_id' => '2'],
        ['username' => 'customer', 'email' => 'customer@gmail.com', 'password' => 'customer', 'role_type_id' => '3'],
    ];

    public function run()
    {
//        foreach ($this->listUser as $item)
//            DB::table('users')->insert([
//                'username' => $item['username'],
//                'email' => $item['email'],
//                'role_type_id' => $item['role_type_id'],
//                'password' => Hash::make($item['password']),
//            ]);
        foreach ($this->listUser as $item)
            DB::table('users')->updateOrInsert(
                [ 'username' => $item['username']],
                [
                    'username' => $item['username'],
                    'email' => $item['email'],
                    'role_type_id' => $item['role_type_id'],
                    'password' => Hash::make($item['password']),
                ]
            );
    }
}
