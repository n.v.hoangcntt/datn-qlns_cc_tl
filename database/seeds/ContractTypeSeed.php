<?php

use Illuminate\Database\Seeder;

class ContractTypeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        foreach (CONTRACT_TYPE as $index => $item)
            DB::table('contract_types')->updateOrInsert(['id' => $index +1],
                ['contract_name' => CONTRACT_TYPE[$index]]);
    }
}
