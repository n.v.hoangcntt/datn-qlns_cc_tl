<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (ROLE as $index => $item)
            DB::table('role_types')->updateOrInsert(['id' => $index + 1],
                ['role_name' => ROLE[$index]
                ]);
    }
}
