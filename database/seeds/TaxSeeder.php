<?php

use Illuminate\Database\Seeder;

class TaxSeeder extends Seeder
{
    protected $list_tax = [
        ['taxation_start' => '0', 'taxation_end' => '11000000', 'percent' => '0', 'tax_down' => '0'],
        ['taxation_start' => '11000000', 'taxation_end' => '18000000', 'percent' => '15', 'tax_down' => '750000'],
        ['taxation_start' => '18000000', 'taxation_end' => '32000000', 'percent' => '20', 'tax_down' => '1650000'],
        ['taxation_start' => '32000000', 'taxation_end' => '52000000', 'percent' => '25', 'tax_down' => '3250000'],
        ['taxation_start' => '52000000', 'taxation_end' => '80000000', 'percent' => '30', 'tax_down' => '5850000'],
        ['taxation_start' => '80000000', 'taxation_end' => '', 'percent' => '35', 'tax_down' => '9850000'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->list_tax as $index => $item)
            DB::table('taxes')->updateOrInsert(
                ['id' => $index + 1],
                [
                    'taxation_start' => $item['taxation_start'],
                    'taxation_end' => $item['taxation_end'],
                    'percent' => $item['percent'],
                    'tax_down' => $item['tax_down']
                ]
            );
    }
}
