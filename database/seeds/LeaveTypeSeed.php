<?php

use Illuminate\Database\Seeder;

class LeaveTypeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (LEAVE_TYPE as $index => $item){
            if ($index == 0 || $item == 4) {
                DB::table('leave_types')->updateOrInsert(['id' => $index + 1],
                    [
                        'leave_type_name' => LEAVE_TYPE[$index],
                        'workday' => 1
                    ]);
            } elseif ($index == 1) {
                DB::table('leave_types')->updateOrInsert(['id' => $index + 1],
                    [
                        'leave_type_name' => LEAVE_TYPE[$index],
                        'workday' => 0.5
                    ]);
            } else {
                DB::table('leave_types')->updateOrInsert(['id' => $index + 1],
                    [
                        'leave_type_name' => LEAVE_TYPE[$index],
                        'workday' => 0
                    ]);
            }

        }
    }
}
