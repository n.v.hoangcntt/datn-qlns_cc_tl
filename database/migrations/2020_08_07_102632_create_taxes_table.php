<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxes', function (Blueprint $table) {
            $table->id();
            $table->string('taxation_start'); // mức bắt đầu bảo hiểm
            $table->string('taxation_end')->nullable();; // mức kết thúc bảo hiểm
            $table->integer('percent'); // phần trăm chịu thuế
            $table->string('tax_down'); // số tiền giảm khi tính
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taxes');
    }
}
