<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contract_type_id'); // id loai hop dong
            $table->string('contract_title')->nullable();
            $table->date('date_start')->nullable(); // ngay bat dau hop dong
            $table->date('date_end')->nullable(); // ngay ket thuc hop dong
            $table->string('content')->nullable();
            $table->string('link')->nullable(); // link luu tru hop dong
            $table->unsignedBigInteger('user_id'); // id cua chu hop dong
            $table->string('salary')->nullable(); // muc luong NET
            $table->string('insurance_discount')->nullable(); //tien bao hiem mien giam
            $table->string('accuracy_link')->nullable(); //link file xác thực
            $table->tinyInteger('percent')->default(100)->nullable(); // phan tram huong luong
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
