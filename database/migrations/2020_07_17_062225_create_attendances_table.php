<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendances', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id'); //mã id nhân viên
            $table->date('attendances_date'); // ngay cham cong
            $table->dateTime('check_in'); // time vao
            $table->dateTime('check_out')->nullable(); // time ra
            $table->integer('late_time')->default(0)->nullable();
            $table->integer('over_time')->default(0)->nullable();
            $table->integer('over_time_push')->default(0)->nullable();
            $table->double('workday',6,2)->default(0)->nullable();
            $table->tinyInteger('status')->default(0);
            $table->unsignedBigInteger('updated_by')->nullable(); // người cập nhập
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendances');
    }
}
