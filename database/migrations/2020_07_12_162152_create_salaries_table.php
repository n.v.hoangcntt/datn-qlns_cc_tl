<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaries', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('month_of_year', 12); // luong cua thang - nam
            $table->integer('work_month')->default(0)->nullable(); // sô ngày làm trong tháng
            $table->string('salary_one_day', 12)->default(0)->nullable();; // luong 1 ngay
            $table->string('allowance')->default(0)->nullable();// Phụ cấp
            $table->string('social_security')->default(0)->nullable();// tiền bhxh thuế trừ đi
            $table->string('insurance_discount')->default(0)->nullable(); //tien bao hiem mien giam, miễn trừ gia cảnh của nhân viên
            $table->string('bonus')->default(0)->nullable();// thưởng
            $table->string('other')->default(0)->nullable();// khoản phí khác
            $table->double('total_over_time')->default(0)->nullable(); // tổng ngày tăng ca
            $table->double('total_over_time_free')->default(0)->nullable(); // tổng ngày tăng ca miễn thuế
            $table->string('money_over_time')->default(0)->nullable();// tiền tăng ca
            $table->string('money_over_time_free')->default(0)->nullable();// tiền tăng ca không tính thuế
            $table->integer('total_late_time')->default(0)->nullable(); // tổng số giờ tới muộn
            $table->integer('average_late_time')->default(0)->nullable(); // trung bình cộng tới muộn
            $table->string('mulct')->default('0')->nullable();// tiền phạt
            $table->double('total_workday',6,2)->default(0)->nullable(); // số công trong tháng
            $table->tinyInteger('status')->default('0')->nullable();// trạng thái
            $table->string('total_salary')->default(0)->nullable(); // tổng lương được nhận
            $table->string('result_salary')->default(0)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salaries');
    }
}
