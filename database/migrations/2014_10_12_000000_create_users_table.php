<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name',50)->nullable();// tên nhân viên
            $table->string('email',50)->unique(); // email nhân viên
            $table->string('username',50)->unique(); // tài khoản của nhân viên
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password'); // mật khẩu tài khoản
            $table->unsignedBigInteger('role_type_id')->nullable(); // id quyền của nhân viên
            $table->string('bank_account',30)->nullable(); // số tài khoản ngân hàng
            $table->string('avatar')->nullable(); // link avatar
            $table->string('gender',20)->nullable(); // giới tính
            $table->string('phone_number',20)->nullable();
            $table->unsignedBigInteger('department_id')->nullable(); // id phòng ban
            $table->decimal('leave_day', 8,2)->default(0)->nullable(); // số ngày nghỉ của nhân viên
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
