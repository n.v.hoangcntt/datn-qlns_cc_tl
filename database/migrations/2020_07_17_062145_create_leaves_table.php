<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaves', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('leave_type_id')->nullable(false); // loai nghi phep
            $table->date('date_leave'); // ngay nghỉ
            $table->string('content')->nullable(); // lý do, nội dung đơn xin nghỉ
            $table->string('accuracy')->nullable(); // file xác thực
            $table->string('leave_group')->nullable(); // thuộc nhóm
            $table->boolean('status')->default(0);
            $table->unsignedBigInteger('updated_by')->nullable(); // người cập nhập
            $table->unsignedBigInteger('user_id'); // id nhân viên xin nghỉ
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaves');
    }
}
