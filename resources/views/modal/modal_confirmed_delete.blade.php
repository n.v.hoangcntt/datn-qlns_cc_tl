<div id="modalConfirmedDelete" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Xác Nhận</h2>
                <button type="button" class="close float-right" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Bạn có thực sự muốn xóa!!</h4>
            </div>
            <div class="modal-footer d-flex justify-content-around">
                <a name="btn_modal_confirmed_delete_ok" id="btn_modal_confirmed_delete_ok" class="btn btn-danger text-white">Đồng ý
                </a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Hủy bỏ</button>
            </div>

        </div>
    </div>
</div>
