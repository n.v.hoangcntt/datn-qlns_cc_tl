
<!-- Modal draggable_modal-->
<div class="modal fade draggable_modal_class" id="view_file" data-backdrop="false" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ViewFileLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg draggable_modal_dialog" role="document">
        <div class="modal-content">
            <div class="modal-header move_modal">
                <h5 class="modal-title" id="title_draggable">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body draggable_modal_body">
{{--                <iframe src='https://view.officeapps.live.com/op/embed.aspx?src=/contract/testexcel.xlsx' width='100%' height='565px' frameborder='0'> </iframe>--}}
                <iframe id="link_file_view" src="" width='100%' height='398px' allowfullscreen webkitallowfullscreen></iframe>
            </div>
            <div class="modal-footer  move_modal d-flex justify-content-around">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
