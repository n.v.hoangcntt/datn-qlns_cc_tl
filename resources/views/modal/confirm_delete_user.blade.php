<div id="confirmModalDeleteUser" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Xác Nhận</h2>
                <button type="button" class="close float-right" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Bạn có thực sự muốn xóa!!</h4>
            </div>
            <form id="delete_modal_user"  name="delete_modal_user" action="{{route('user_management.index')}}" method="POST" style="display: none;">
                @csrf
                <input type="hidden" name="_method" value="delete">
                <input type="hidden" name="delete_id" id='delete_id'>
            </form>
            <div class="modal-footer d-flex justify-content-around">
                <button type="button" name="button_confirm_delete_ok" id="button_confirm_delete_ok" class="btn btn-danger">Đồng ý
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Hủy bỏ</button>
            </div>

        </div>
    </div>
</div>
