@if ($errors->any())
    <div id="notification" class="notification">
        <!-- Modal content -->
        <div class="notification-content" style="min-height: 30px">
            <span class="notification-close notification-close-icon">&times;</span>
            <div class="body-notification">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="min-height: 30px">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close btn-100" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="footer-notification d-flex justify-content-center">
                <button class="btn-success notification-close" style="padding: 6px; border-radius: 5px"> Xác nhận</button>
            </div>

        </div>
    </div>
@elseif(Session::has('success'))
    <div id="notification" class="notification">
        <!-- Modal content -->
        <div class="notification-content" style="min-height: 30px">
            <span class="notification-close notification-close-icon btn-100">&times;</span>
            <div class="body-notification">
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="min-height: 30px">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="close btn-100" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="footer-notification d-flex justify-content-center">
                <button class="btn-success notification-close" style="padding: 6px; border-radius: 5px"> Xác nhận</button>
            </div>

        </div>
    </div>
@elseif(Session::has('error'))
    <div id="notification" class="notification">
        <!-- Modal content -->
        <div class="notification-content">
            <span class="notification-close notification-close-icon btn-100">&times;</span>
            <div class="body-notification">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="min-height: 30px">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="close btn-100" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="footer-notification d-flex justify-content-center">
                <button class="btn-success notification-close" style="padding: 6px; border-radius: 5px"> Xác nhận</button>
            </div>

        </div>
    </div>
@endif

