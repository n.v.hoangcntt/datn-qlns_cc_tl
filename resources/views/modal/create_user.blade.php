<div id="formModalCreateUser" name="formModalCreateUser" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <span id="form-alert"></span>
                <form method="post" id="userForm" action="{{route("user_management.store")}}"
                      class="form-horizontal form-row" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="_method" id='_method' value="post">
                    <input type="hidden" name="id" id="id">

                    <div class="form-group col-md-6">
                        <label for="username" class="control-label col-md-12">UserName<span
                                class="text-danger"> *</span> :</label>
                        <div class="col-md-12">
                            <input type="text" name="username" id="username" class="username form-control"/>
                            <p style="color:red; display: none" class="error errorUsername"></p>
                        </div>

                        <label for="email" class="control-label col-md-4">Email<span
                                class="text-danger"> *</span>:</label>
                        <div class="col-md-12">
                            <input type="text" name="email" id="email" class="form-control"/>
                            <p style="color:red; display: none" class="error errorEmail"></p>

                        </div>

                    </div>

                    <div class="form-group col-md-6">
                        <label for="avatar" class="control-label col-md-12">Select Profile Image : </label>
                        <div class="col-md-12 d-flex justify-content-center">
                            <input type="file" name="avatar" id="avatar" hidden
                                   accept="image/*"
                                   onchange="document.getElementById('showiamge').src = window.URL.createObjectURL(this.files[0])"/>
                            <!--  onchange="document.getElementById('showiamge').src = window.URL.createObjectURL(this.files[0])" de input hien thi anh -->
                            <p style="color:red; display: none" class="error errorImage"></p>
                            <img onclick="document.getElementById('avatar').click()" id="showiamge"
                                 data-srcDefault="{{asset('images/flag_vn.png')}}"
                                 src="{{asset('images/flag_vn.png')}}" alt="your avatar upload" height="100"/>
                            <span id="store_image"></span>
                        </div>
                    </div>


                    <div class="form-group col-md-6">
                        <label for="password" class="control-label col-md-12">Mật khẩu <span
                                class="text-danger"> *</span>: </label>
                        <div class="col-md-12">
                            <input type="text" name="password" id="password" class="form-control"/>
                            <p style="color:red; display: none" class="error errorPassword"></p>

                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="confirm_password" class="control-label col-md-12">Xác nhận mật khẩu <span
                                class="text-danger"> *</span>: </label>
                        <div class="col-md-12">
                            <input type="text" name="password_confirmation" id="password_confirmation"
                                   class="form-control"/>
                            <p style="color:red; display: none" class="error confirmPassword"></p>

                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="name" class="control-label col-md-12">Họ & tên : </label>
                        <div class="col-md-12">
                            <input type="text" name="name" id="name" class="form-control"/>
                            <p style="color:red; display: none" class="error errorName"></p>

                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="username" class="control-label col-md-12">Vai trò
                            <span class="text-danger"> *</span></label>
                        <div class="col-md-12">
                            <select class="form-control" id="role_type_id" name="role_type_id">
                                @foreach(getListRole() as $role)
                                    <option
                                        value="{{$role->id}}" {{$role->id == 3 ? 'selected' : ''}}>{{$role->role_name}}</option>
                                @endforeach
                            </select>
                            <p style="color:red; display: none" class="error errorRole"></p>
                        </div>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="gender" class="control-label col-md-12">Giới tính:
                            <span class="text-danger"> *</span></label>
                        <div class="col-md-12">
                            <select class="form-control" id="gender" name="gender">
                                <option value="Nam" selected>Nam</option>
                                <option value="Nữ">Nữ</option>

                            </select>
                            <p style="color:red; display: none" class="error errorRole"></p>
                        </div>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="bank_account" class="control-label col-md-12">Phòng ban: </label>
                        <div class="col-md-12">
                            <select class="form-control" id="department_id" name="department_id">
                                <option value="">Chưa có</option>
                                @foreach(getListDepartment() as $item)
                                    <option
                                        value="{{$item->id}}">{{$item->department_name}}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="bank_account" class="control-label col-md-12">Tài khoản ngân hàng: </label>
                        <div class="col-md-12">
                            <input type="text" name="bank_account" id="bank_account" class="form-control"/>
                            <p style="color:red; display: none" class="error errorBankAccount"></p>

                        </div>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="phone_number" class="control-label col-md-12">Số điện thoại: </label>
                        <div class="col-md-12">
                            <input type="text" name="phone_number" id="phone_number" class="form-control"/>
                            <p style="color:red; display: none" class="error errorBankAccount"></p>

                        </div>
                    </div>

                    <div class="form-group col-md-12 d-flex justify-content-around">
                        <button type="submit" name="action_user" id="action_user" class="btn btn-primary col-3">Thêm
                        </button>

                        <button type="button" name="cancel" id="cancel" data-dismiss="modal"
                                class="btn btn-danger col-3"> Hủy
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

