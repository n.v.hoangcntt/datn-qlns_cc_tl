<div id="ThemHopDongModal" name="ThemHopDongModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <span id="form-alert"></span>
                <form method="post" id="contractForm" action="{{route("contractFile.store")}}"
                      class="form-horizontal form-row" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="_method" id='_method' value="post">
                    <input type="hidden" name="id" id="id"/>
                    <input type="hidden" name="user_id" id="contract_user_id" />

                    <div class="form-group col-md-12">
                        <label for="link" class="control-label col-md-12">File hợp đồng: </label>
                    </div>
                    <div class="form-group col-md-8">
                        <div class="col-md-12">
                                <input style="border: 1px solid #ced4da; border-radius: .25rem; width: 100%" type="file" accept=".pdf,image/*" name="link" id="link">
                        </div>
                    </div>


                    <div class="form-group col-md-4" id="div_file_contract">
                        <div class="col-md-12 d-flex justify-content-around" id="actionFile" style="height: 36px">
                            <a  type="button" name="xem_hop_dong" id="xem_hop_dong" class="btn btn-primary btn_view_file">Xem</a>

                            <a type="button" name="tai_hop_dong" id="tai_hop_dong" class="btn btn-success" download>Tải về</a>

                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="contract_title" class="control-label col-md-12">Ngày Bắt đầu - Ngày kết thúc
                            <span class="text-danger"> *</span> :</label>
                        <div class="col-md-12 input-group input-daterange datepicker d-flex justify-content-between px-4" data-provide="datepicker">
                            <input  name="date_start" id="date_start" type="text" class="form-control datepicker col-md-5">
                            <div class="input-group-addon"> tới </div>
                            <input type="text" class="form-control datepicker col-md-5" id="date_end" name="date_end" data-date-format="dd/mm/yyyy">
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="contract_type_id" class="control-label col-md-12">Loại hợp đồng
                            <span class="text-danger"> *</span></label>
                        <div class="col-md-12">
                            <select class="form-control" id="contract_type_id" name="contract_type_id">
                                @foreach(getListContractType() as $contract)
                                    <option
                                        value="{{$contract->id}}" {{$contract->id == 1 ? 'selected' : ''}}>{{$contract->contract_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="salary" class="control-label col-md-12">Lương  <span class="text-danger"> *</span> : </label>
                        <div class="col-md-12">
                            <input type="number" name="salary" id="salary" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="insurance_discount" class="control-label col-md-12">Mức miễn trừ: </label>
                        <div class="col-md-12">
                            <input type="number" name="insurance_discount" id="insurance_discount" class="form-control"/>
                        </div>
                    </div>


                    <div class="form-group col-md-6">
                        <label for="insurance_discount" class="control-label col-md-12">Xác thực: </label>
                        <div class="col-md-12 row">
                            <div class="col-md-7">
                            <input type="file"  style="border: 1px solid #ced4da; border-radius: .25rem; width: 100%" name="accuracy_link" accept=".pdf,image/*">
                            </div>
                            <div class="form-group col-md-5  p-0" id="div_file_accuracy">
                                <div class="col-md-12 d-flex justify-content-around p-0" id="actionFileAccuracy">
                                    <a  type="button" name="xem_xac_thuc" id="xem_xac_thuc" style="font-size: 12px" class="btn btn-primary btn_view_file">Xem</a>

                                    <a type="button" name="tai_xac_thuc" id="tai_xac_thuc" style="font-size: 12px" class="btn btn-success" download>Tải về</a>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-12 d-flex justify-content-around">
                        <button type="submit" name="action_contract" id="action_user" class="btn btn-primary col-3">Thêm</button>

                        <button type="button" data-dismiss="modal" class="btn btn-danger col-3"> Hủy</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

