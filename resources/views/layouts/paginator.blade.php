<div class="justify-content-between" style="display: flex;">
    @if(count($paginator) > 0)
        <div style="line-height:40px">
            {!! $paginator->firstItem() . ' ~ ' . $paginator->lastItem() . ' (' . $paginator->total() .' '. trans('pagination.record'). ' )' !!}
        </div>
    @endif
    @if ($paginator->hasPages())
        <div>
            <nav aria-label="{{trans('pagination.Pagination')}}">
                <ul class="pagination justify-content-end">
                    {{-- Previous Page Link --}}
                    @if ($paginator->currentPage() > 1)
                        <li class="page-item" aria-label="{{trans('pagination.First')}}">
                            <a href="{{ $paginator->url(1) }}" rel="frist" aria-label="{{trans('pagination.First')}}"><span class="page-link page-laravel">&laquo;</span></a>
                        </li>
                        @if ($paginator->currentPage() == 2)
                            <li class="page-item">
                                <a href="{{ $paginator->url(1) }}" rel="prev" aria-label="{{trans('pagination.Prev')}}"><span class="page-link page-laravel">&lsaquo;</span></a>
                            </li>
                        @else
                            <li class="page-item">
                                <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="{{trans('pagination.Prev')}}"><span class="page-link page-laravel">&lsaquo;</span></a>
                            </li>
                        @endif
                    @endif

                    {{-- Pagination Elements --}}
                    @if($paginator->currentPage() > 3)
                        <li class="page-item"><a href="{{ $paginator->url(1) }}"><span class="page-link page-laravel">1</span></a></li>
                    @endif
                    @if($paginator->currentPage() > 4)
                        <li class="page-item"><a><span class="page-link page-laravel">...</span></a></li>
                    @endif
                    @foreach(range(1, $paginator->lastPage()) as $i)
                        @if($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2)
                            @if($i == $paginator->currentPage())
                                <li class="page-item active"><a href="{{ $paginator->url($i) }}"><span class="page-link page-laravel">{{ $i }}</span></a></li>
                            @else
                                <li class="page-item"><a href="{{ $paginator->url($i) }}"><span class="page-link page-laravel">{{ $i }}</span></a></li>
                            @endif
                        @endif
                    @endforeach
                    @if($paginator->currentPage() < $paginator->lastPage() - 3)
                        <li class="page-item"><a><span class="page-link page-laravel">...</span></a></li>
                    @endif
                    @if($paginator->currentPage() < $paginator->lastPage() - 2)
                        <li class="page-item"><a href="{{ $paginator->url($paginator->lastPage()) }}"><span class="page-link page-laravel">{{ $paginator->lastPage() }}</span></a></li>
                    @endif

                    {{-- Next Page Link --}}
                    @if ($paginator->hasMorePages())
                        <li class="page-item">
                            <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="{{trans('pagination.Next')}}"><span class="page-link page-laravel">&rsaquo;</span></a>
                        </li>
                        <li class="page-item" aria-label="{{trans('pagination.Last')}}">
                            <a href="{{ $paginator->toArray()['last_page_url'] }}" rel="last" aria-label="{{trans('pagination.Last')}}"><span class="page-link page-laravel">&raquo;</span></a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    @endif
</div>
