<!--END HEADER-->
<section class="sidebar">
    <div class="sidebar-cont">
    </div>

    <h2>Navigation</h2>
    <ul class="navigation">
        <li class="{{activeRoute(URI_DASHBOARD)}}"> <a href="{{route('dashboard')}}"><i class="iconsminds-dashboard"></i> {{__('layout.Dashboard')}}</a></li>
        <li class="{{activeRoute(LEAVE_ATTENDANCE)}}"><a href="{{route('leave_attendance')}}"><i class="iconsminds-coins"></i>{{__('layout.Leave & Attendance Management')}}</a></li>
        <li class="{{activeRoute(URI_USER_MANAGEMENT)}}"><a href="{{route('user_management.index')}}"><i class="iconsminds-male-female"></i>{{__('layout.User Management')}} </a></li>
        <li class="{{activeRoute(REPORTS)}}"><a href="{{route('indexReport')}}"><i class="iconsminds-pie-chart"></i> {{__('layout.Report')}}</a></li>
        <li  class="{{activeRoute(URI_SETTING)}}"><a href="{{route('setting')}}"><i class="iconsminds-gears"></i> {{__('layout.Setting')}}</a></li>
    </ul>
</section>
<!--END SIDEBAR-->
