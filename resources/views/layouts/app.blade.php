<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{asset('images/flag_vn.png')}}" type="image/x-icon" sizes="32x32"/>
    <meta name="keywords"
          content=""/>
    <meta name="description" content=""/>


    <!-- CSRF Token -->

    <title>{{ config('app.name', 'DATN') }}</title>

{{--    <!-- Fonts -->--}}
{{--    <link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
{{--    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">--}}
    <title>
        @section('title')
            DATNByHoang
        @show
        - {{ config('app.name', 'DATN') }}</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap-4.3.1.min.css')}}" crossorigin="anonymous">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{'css/color.css'}}">
    <link rel="stylesheet" href="{{asset('fonts/iconsmind/css/iconsminds.css')}}">
    <link rel="stylesheet" href="{{asset('css/common.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link href="{{ asset('css/custom-style-user.css') }}" rel="stylesheet">

        <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('datetimepicker/jquery.datetimepicker.css')}}">

    <script src="{{asset('js/jquery-3.4.1.min.js')}}" ></script>
    <script src="{{asset('js/popper1.14.7.min.js')}}" ></script>
    <script src="{{asset('js/bootstrap-4.3.1.min.js')}}"
             ></script>
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('js/script.js')}}" ></script>
    <script src="{{asset('js/notification.js')}}" defer ></script>


{{--    datetimnepicker--}}


    <script src="{{asset('datetimepicker/build/jquery.datetimepicker.full.min.js')}}" ></script>
        <script src="{{asset('js/bootstrap-datepicker.min.js')}}" ></script>
    <script src="{{asset('js/common.js')}}" ></script>
</head>
<body id="top">
@include('modal.modal_notification')
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm" style="height: 56px; background-color: #398ee0">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{route('dashboard') }}" style="font-size: 20px">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('auth.Login') }}</a>
                        </li>
                        {{--                            @if (Route::has('register'))--}}
                        {{--                                <li class="nav-item">--}}
                        {{--                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                        {{--                                </li>--}}
                        {{--                            @endif--}}
                    @else
                        <li class="nav-item {{activeRoute('home')}} nav-action">
                            <a class="nav-link" href="{{route('home')}}">Trang chủ <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item  {{activeRoute('leave')}} nav-action">
                            <a class="nav-link" href="{{route('leave')}}">Nghỉ phép</a>
                        </li>
                        <li class="nav-item {{activeRoute('attendance')}} nav-action mr-3">
                            <a class="nav-link" href="{{route('attendance')}}">Chấm công</a>
                        </li>

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right navbar-menu-custom"
                                 aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('auth.Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="p-4">
        @yield('content')

    </main>
    @include('modal.modal_confirmed_delete')
</div>

<a href="#top" class="go-to-topppppp smooth" style="background-color: red"></a>
@yield('after_js')
</body>
</html>
