<!doctype html>
<html lang=en-US class=no-js>
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{asset('images/flag_vn.png')}}" type="image/x-icon" sizes="32x32"/>
    <meta name="keywords"
          content=""/>
    <meta name="description" content=""/>
    <title>
        @section('title')
            DATNByHoang
        @show
        - {{ config('app.name', 'DATN') }}</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap-4.3.1.min.css')}}" crossorigin="anonymous">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{'css/color.css'}}">
    <link rel="stylesheet" href="{{asset('fonts/iconsmind/css/iconsminds.css')}}">
    <link rel="stylesheet" href="{{asset('css/common.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('datetimepicker/jquery.datetimepicker.css')}}" defer>

    <script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
    <script src="{{asset('datetimepicker/build/jquery.datetimepicker.full.min.js')}}"></script>
    <script src="{{asset('js/jquery-dateformat.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('js/popper1.14.7.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-4.3.1.min.js')}}"
            defer ></script>
    <script src="{{asset('js/script.js')}}" defer></script>
    <script src="{{asset('js/notification.js')}}" defer></script>
    <script src="{{asset('js/bootstrap-datepicker.min.js')}}" ></script>
    <script src="{{asset('js/common.js')}}" ></script>


    @yield('after_css')
    @yield('before_js')
</head>
<body id="top" class="BgWeface">
@include('modal.draggable_modal')
@include('modal.modal_notification')
<div class="wrapper open_sidebar">
    <header>
        <button class="btn_nav_sm">
            <div class="nav_sm_container">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <a class="brand" href="{{ route('dashboard') }}" style="padding-top: 10px">
            {{ config('app.name', 'Laravel') }}
        </a>
        {{--        <a href="#" class="brand"><img src="images/logo.png" alt="Weface"></a>--}}
        <div class="head-content">
            <!--                <a href="#" class="lang"><img src="images/flag_en.png" alt="en"></a>-->
            <!--                <a href="#" class="btn-trial" data-toggle="modal" data-target="#changePlanModal">Trial 30 days</a>-->
            <div class="user-area-head">

                <div class="dropdown">
                    <div class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="logo-store">
                            <img src="images/avatar.png" alt="store logo">
                        </div>
                        <div class="store-name">{{auth()->user()->name}}</div>
                    </div>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
{{--                        <a class="dropdown-item" href="profile.html">Hồ sơ</a>--}}
                        <a class="dropdown-item" href="#">Đổi mật khẩu</a>
                        <a class="dropdown-item" href="{{route('getLogout')}}">Thoát</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    @include('layouts.sidebar')
    <section class="content management-page">
        <div class="section-header">
            <h1 class="page-title"> @yield('title_page')</h1>
            @yield('section_header')
        </div>
        @yield('content')
    </section>
    @include('layouts.footer')
    <a href="#top" class="go-to-topppppp smooth"></a>
</div>

@yield('after_js')

@include('modal.modal_confirmed_delete')
</body>
</html>
