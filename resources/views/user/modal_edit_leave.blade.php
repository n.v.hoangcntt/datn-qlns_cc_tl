<div id="modalEditLeave" name="modalEditLeave" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="editLeaveForm" action="{{route('leaveUpdate')}}"
                      class="form-row d-flex justify-content-sm-around" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="_method" id='_method' value="put">
                    <input type="hidden" name="id" id="leave_id">

                    <div class="form-group col-md-3 row">
                        <label for="date_leave" class="control-label col-md-12">Ngày nghỉ:
                            <span class="text-danger">*</span></label>
                        <div class="col-md-12">
                            <input class="form-control" name="date_leave" id="date_leave_modal" readonly>
                        </div>
                    </div>
                    <div class="form-group col-md-3 row">
                        <label for="leave_type_id_modal" class="control-label col-md-12">Loại nghỉ phép:
                            <span class="text-danger">*</span></label>
                        <div class="col-md-12">
                            <select class="form-control" id="leave_type_id_modal" name="leave_type_id">
                                @foreach(getListLeaveType() as $listLeave)
                                    <option
                                        value="{{$listLeave->id}}" {{$listLeave->id == 1 ? 'selected' : ''}}>{{$listLeave->leave_type_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6 row">
                        <label for="content" class="control-label col-md-12">Lý do:
                        </label>
                        <div class="col">
                            <input name="content" id="content_modal" class="form-control" type="text">
                        </div>
                    </div>

                    <div class="form-group col-md-12 row justify-content-end">
                        <div class="form-group col-md-12 d-flex justify-content-around">
                            <button type="submit" name="action_user" id="action_user" class="btn btn-primary col-3">
                                Sửa
                            </button>

                            <button type="button" name="cancel" id="cancel" data-dismiss="modal"
                                    class="btn btn-danger col-3"> Hủy
                            </button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
