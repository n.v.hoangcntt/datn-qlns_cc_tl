@extends('layouts.app')

@section('content')
    <div class="pb-4">
        <h2>NGHỈ PHÉP
            <div class="float-right">
                @if(checkQuanly())
                    <a class="btn btn-primary" style="padding: 5px 5px; font-size: 12px"
                       href="{{route('manage_leave')}}">Quản
                        lý nghỉ phép nhân viên</a>
                @endif
            </div>
        </h2>
    </div>
    <div class="row card-group">
        <div class="col-12 d-flex">
            <div class="card flex-fill div-card">
                {{--Form xin nghỉ--}}
                <div class="card-body" style="min-height: 88vh">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-md-start justify-content-md-start">
                                <div class="col-md-2"><strong> Xin nghỉ phép</strong></div>
                                <div class="col-md -10">
                                    <ul class="nav nav-pills " id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill"
                                               href="#oneDay" role="tab" aria-controls="pills-home"
                                               aria-selected="true">
                                                Nghỉ một ngày</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill"
                                               href="#multiDay" role="tab" aria-controls="pills-profile"
                                               aria-selected="false">
                                                Nghỉ dài ngày</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="pills-tabContent" class="tab-content" style="border: none">
                                {{--                                 nghỉ một ngày--}}
                                <div class="tab-pane fade show active" id="oneDay" role="tabpanel"
                                     aria-labelledby="pills-onday-tab">
                                    <form method="post" id="leaveForm" action="{{route('leaveStore')}}"
                                          class="form-row d-flex justify-content-sm-around"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="_method" id='_method' value="post">

                                        <div class="form-group col-md-3 row">
                                            <label for="date_leave" class="control-label col-md-12">Ngày nghỉ:
                                                <span class="text-danger">*</span></label>
                                            <div class="col-md-12">
                                                <input class="form-control" name="date_leave" id="date_leave"
                                                       data-provide="datepicker">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3 row">
                                            <label for="leave_type_id" class="control-label col-md-12">Loại nghỉ phép:
                                                <span class="text-danger">*</span></label>
                                            <div class="col-md-12">
                                                <select class="form-control" id="leave_type_id_multiple" name="leave_type_id">
                                                    @foreach(getListLeaveType() as $listLeave)
                                                        @if($listLeave->id != 4)
                                                        <option
                                                            value="{{$listLeave->id}}" {{$listLeave->id == 1 ? 'selected' : ''}}>{{$listLeave->leave_type_name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6 row">
                                            <label for="content" class="control-label col-md-12">Lý do:
                                            </label>
                                            <div class="col">
                                                <input name="content" id="content" class="form-control" type="text">
                                            </div>
                                        </div>

                                        <div class="form-group col-md-12 row justify-content-end">
                                            <button type="submit" class="btn btn-primary col-md-1"
                                                    style="margin-right: 12px ">
                                                Gửi
                                            </button>
                                        </div>

                                    </form>
                                </div>
                                {{--                                nghỉ dài ngày--}}
                                <div class="tab-pane fade" id="multiDay" role="tabpanel"
                                     aria-labelledby="pills-mutilday-tab">
                                    <form method="post" id="leaveForm" action="{{route('leaveStoreMultiDay')}}"
                                          class="form-row d-flex justify-content-sm-around"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="_method" id='_method' value="post">
                                        <div class="form-group col-md-2 row">
                                            <label for="leave_type_id_multiple" class="control-label col-md-12">Loại nghỉ phép:
                                                <span class="text-danger">*</span></label>
                                            <div class="col-md-12">
                                                <select class="form-control" id="leave_type_id_multiple" onchange="onchangeDate(this)" name="leave_type_id">
                                                    @foreach(getListLeaveType() as $listLeave)
                                                        @if($listLeave->id != 2)
                                                            <option value="{{$listLeave->id}}" {{$listLeave->id == 1 ? 'selected' : ''}}>{{$listLeave->leave_type_name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row col-md-4 d-flex justify-content-end">
                                            <label for="date_leave" class="control-label col-md-12">Ngày nghỉ:
                                                <span class="text-danger">*</span></label>
                                            <div class="col-md-5">
                                                <input class="form-control" name="date_leave_start"
                                                       id="date_timepicker_start" type="text" autocomplete="off">
                                            </div>
                                            <div style="padding-top: 5px"> Tới</div>

                                            <div class="col-md-5">
                                                <input class="form-control" name="date_leave_end"
                                                       id="date_timepicker_end" type="text" autocomplete="off">
                                            </div>
                                        </div>

                                        <div class="form-group col-md-4 row">
                                            <label for="content" class="control-label col-md-12">Lý do:
                                            </label>
                                            <div class="col">
                                                <input name="content" id="content" class="form-control" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-2" id="xacminh">
                                            <label for="content" class="control-label col-md-12">Xác minh <span
                                                    class="text-danger">*</span>:
                                            </label>
                                            <input type="file" style="border: 1px solid #ced4da; border-radius: .25rem; width: 100%" name="accuracy" accept=".pdf,image/*">

                                        </div>
                                        <div class="form-group col-md-12 row justify-content-end">
                                            <button type="submit" class="btn btn-primary col-md-1"
                                                    style="margin-right: 12px ">
                                                Gửi
                                            </button>
                                        </div>

                                    </form>
                                </div>

                            </div>


                        </div>
                    </div>

                    <div class="card flex-fill">
                        <div class="card-body row">
                            <div class="col-md-6 m10b">
                                <form name="form_search" class="form-row" id="form_leave_search"
                                      action="{{route('leave')}}"
                                      method="get">
                                    <input type="text" class="form-control col-md-7 ml-5" name="leave_search"
                                           value="{{ request()->leave_search }}"
                                           id="leave_search" autocomplete="leave_search" placeholder="Nội dung ...">
                                    <button type="submit" class="btn btn-success col-md-3 offset-1" id="button_search"
                                            name=button_search" type="button"
                                            aria-expanded="false">
                                        <i class="fa fa-search"> </i> Tìm
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                    {{--    leave table--}}
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive table-user">
                                @if(!empty($leaves))
                                    {!! $leaves->appends(request()->all())->links('layouts.paginator') !!}
                                @endif
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col" style="width: 5%">#ID</th>
                                        <th scope="col" style="width: 12%">UserName</th>
                                        <th scope="col" style="width: 15%">Email</th>
                                        <th style="width: 15%" scope="col">Số điện thoại</th>
                                        <th scope="col" style="width: 10%">Ngày Nghỉ</th>
                                        <th scope="col">Lý do</th>
                                        <th scope="col" style="width: 160px">Trạng thái</th>
                                        <th class="text-center" scope="col" style="width: 160px">Thao tác</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($leaves as $leave)

                                        <tr>
                                            <th scope="row">{{$leave->id}}</th>
                                            <td>{{$leave->user->username}}</td>
                                            <td>{{$leave->user->email}}</td>
                                            <td>{{$leave->user->phone_number}}</td>
                                            <td>{{$leave->date_leave}}</td>
                                            <td><div class="hiden-td-table">{{$leave->content}}</div></td>
                                            <td>
                                                @if($leave->date_leave)
                                                    Chưa được duyệt
                                                @else
                                                    Đã được duyệt
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if(!$leave->status && strtotime(date("Y-m-d")) <= strtotime($leave->date_leave))
                                                    <a class="btn btn-success btn_edit_leave"
                                                       href="#"
                                                       data-id="{{$leave->id}}" data-data="{{$leave}}"
                                                       data-toggle="modal"
                                                       data-target="#modalEditLeave"
                                                       style="border-left: none"
                                                    >Sửa</a>


                                                    <a class="btn btn-outline-danger text-delete btn_confirmed_delete"
                                                       data-id="{{$leave->id}}" data-target="#modalComfirmedDelete"
                                                       href="{{route('leaveDelete', $leave->id)}}">Xóa</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="8" class="text-center text-secondary">Không có dữ liệu!</td>
                                        </tr>
                                    @endforelse

                                    </tbody>
                                </table>
                                @if(!empty($leaves))
                                    {!! $leaves->appends(request()->all())->links('layouts.paginator') !!}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end card-->
    </div>
    @include('user.modal_edit_leave')
@section('after_js')
    <script language="javascript">

        function onchangeDate(select){
            const dateNow = new Date();
            let valueSelect = select.value;
            if(valueSelect == 4 ){
                $('#xacminh').css('display', 'block');
                jQuery(function(){
                    jQuery('#date_timepicker_start').datetimepicker({
                        format:'Y-m-d',
                        onShow:function( ct ){
                            this.setOptions({
                                maxDate:jQuery('#date_timepicker_end').val()?jQuery('#date_timepicker_end').val():false,
                                minDate:false,
                            })
                        },
                        onChangeDateTime:function(current_time,$input){
                            jQuery('#date_timepicker_end').val()?jQuery('#date_timepicker_end').val():jQuery('#date_timepicker_end').val( jQuery('#date_timepicker_start').val())
                        },
                        timepicker:false
                    });
                    jQuery('#date_timepicker_end').datetimepicker({
                        format:'Y-m-d',
                        onShow:function( ct ){
                            this.setOptions({
                                minDate:jQuery('#date_timepicker_start').val()?jQuery('#date_timepicker_start').val():false
                            })
                        },
                        onChangeDateTime:function(current_time,$input){
                            jQuery('#date_timepicker_start').val()?jQuery('#date_timepicker_end').val():jQuery('#date_timepicker_start').val( jQuery('#date_timepicker_end').val())
                        },
                        timepicker:false
                    });
                });
            }else{
                $('#xacminh').css('display', 'none');
                jQuery(function(){
                    jQuery('#date_timepicker_start').datetimepicker({
                        format:'Y-m-d',
                        onShow:function( ct ){
                            this.setOptions({
                                maxDate:jQuery('#date_timepicker_end').val()?jQuery('#date_timepicker_end').val():false,
                                minDate: new Date()
                            })
                        },
                        onChangeDateTime:function(current_time,$input){
                            jQuery('#date_timepicker_end').val()?jQuery('#date_timepicker_end').val():jQuery('#date_timepicker_end').val( jQuery('#date_timepicker_start').val())
                        },
                        timepicker:false
                    });
                    jQuery('#date_timepicker_end').datetimepicker({
                        format:'Y-m-d',
                        onShow:function( ct ){
                            this.setOptions({
                                minDate:jQuery('#date_timepicker_start').val()?jQuery('#date_timepicker_start').val():false
                            })
                        },
                        onChangeDateTime:function(current_time,$input){
                            jQuery('#date_timepicker_start').val()?jQuery('#date_timepicker_end').val():jQuery('#date_timepicker_start').val( jQuery('#date_timepicker_end').val())
                        },
                        timepicker:false
                    });
                });
            }


        }

    </script>
    <script src="{{asset('js/user/leave.js')}}" defer></script>
@endsection
@endsection


