@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">Thông tin cá nhân</div>
                    <div class="card-body" style="min-height: 68vh">
                        <div class="row">
                            <div class="col-md-12 row">

                                <div class=" col-md-7 offset-sm-1 row">
                                    <strong class="col-md-4" style="width: 180px">Họ & tên</strong>
                                    <p class="col-md-8 text-left"><strong>: </strong>{{auth()->user()->name}}</p>
                                    <strong class="col-md-4" style="width: 180px">UserName</strong>
                                    <p class="col-md-8 text-left"><strong>: </strong>{{auth()->user()->username}}</p>
                                </div>

                                <div class=" col-md-4">
                                    <div class="col-md-12 d-flex justify-content-center">
                                        <img
                                            src="{{auth()->user()->avatar ? asset(auth()->user()->avatar) : asset('images/flag_vn.png')}}"
                                            alt="your avatar "
                                            height="100"/>
                                    </div>
                                </div>
                                {{--  end avatr--}}

                                <div class=" col-md-7 offset-sm-1 row pt-1">
                                    <strong class="col-md-4" style="width: 180px">Email</strong>
                                    <p class="col-md-7 text-left"><strong>: </strong>{{auth()->user()->email}}
                                        @if(auth()->user()->email)
                                            <a href="mailto:{{auth()->user()->email}}"><i class="fa fa-envelope text-primary" aria-hidden="true"></i></a>
                                        @endif
                                    </p>
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-7 offset-sm-1 row pt-1 ">
                                    <strong class="col-md-4" style="width: 180px">Số điện thoại</strong>
                                    <p class="col-md-8 text-left"><strong>: </strong>{{auth()->user()->phone_number}}
                                        @if(auth()->user()->phone_number)
                                            <a href="tel:{{auth()->user()->phone_number}}"><i class="fa fa-phone text-primary" aria-hidden="true"></i></a>
                                        @endif
                                    </p>
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-7 offset-sm-1 row pt-1 ">
                                    <strong class="col-md-4" style="width: 180px">Giới tính</strong>
                                    <p class="col-md-8 text-left"><strong>: </strong>{{auth()->user()->gender}}</p>
                                </div>
                                <div class="col-md-4"></div>
                                <div class=" col-md-7 offset-sm-1 row pt-1">
                                    <strong class="col-md-4" style="width: 180px">Tài khoản ngân hàng</strong>
                                    <p class="col-md-7 text-left"><strong>: </strong>{{auth()->user()->bank_accout}}</p>
                                </div>
                                <div class="col-md-4"></div>
                                <div class=" col-md-7 offset-sm-1 row pt-1">
                                    <strong class="col-md-4" style="width: 180px" >Phòng ban</strong>
                                    <p class="col-md-8 text-left"><strong>: </strong>{{auth()->user()->department && auth()->user()->department->department_name }}</p>
                                </div>
                                <div class=" col-md-7 offset-sm-1 row pt-1">
                                    <strong class="col-md-4" style="width: 180px" >Vai trò</strong>
                                    <p class="col-md-8 text-left"><strong>: </strong>{{auth()->user()->role->role_name}}</p>
                                </div>

                                <div class=" col-md-7 offset-sm-1 row pt-1">
                                    <strong class="col-md-4" style="width: 180px">Ngày nghỉ còn lại</strong>
                                    <p class="col-md-8 text-left"><strong>: </strong>{{auth()->user()->role->leave_day}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card" id="card_chart" style="display: none">
        <div class="card-header text-center">Biểu đồ lương</div>
        <div class="card-body">
            <div id="chartdiv"></div>
        </div>
    </div>
@section('after_js')

    <style>
        #chartdiv {
            width: 100%;
            height: 500px;
        }

    </style>

    <!-- Resources -->
    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
    <script src="//www.amcharts.com/lib/4/lang/vi_VN.js"></script>

    <!-- Chart code -->
    <script>
        $(document).ready(function () {
             const SITEURL = '{{URL::to('')}}';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'GET',
                url: SITEURL + '/test',
                success: function (data) {
                    if(data){
                       $('#card_chart').css('display','block')
                       createChart(data);
                   }else{
                       $('#card_chart').css('display','none')
                   }
                },
                error: function (data) {
                    console.log('loi', $.parseJSON(data.responseText))
                    $('#card_chart').css('display','none')
                }
            });
        });
        function createChart(data){
            let value = JSON.parse(data);
            am4core.ready(function() {
// Themes begin

                am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
                var chart = am4core.create("chartdiv", am4charts.XYChart);

// Add data
                chart.data =value;
// Set input format for the dates
                chart.dateFormatter.inputDateFormat = "yyyy-MM-dd";

// Create axes
                var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "value";
                series.dataFields.dateX = "date";
                series.tooltipText ="Lương {date}: [bold]{valueY} VNĐ[/]";
                series.strokeWidth = 2;
                series.minBulletDistance = 15;

// Drop-shaped tooltips
                series.tooltip.background.cornerRadius = 20;
                series.tooltip.background.strokeOpacity = 0;
                series.tooltip.pointerOrientation = "vertical";
                series.tooltip.label.minWidth = 40;
                series.tooltip.label.minHeight = 40;
                series.tooltip.label.textAlign = "middle";
                series.tooltip.label.textValign = "middle";

// Make bullets grow on hover
                var bullet = series.bullets.push(new am4charts.CircleBullet());
                bullet.circle.strokeWidth = 2;
                bullet.circle.radius = 4;
                bullet.circle.fill = am4core.color("#fff");

                var bullethover = bullet.states.create("hover");
                bullethover.properties.scale = 1.3;

// Make a panning cursor
                chart.cursor = new am4charts.XYCursor();
                chart.cursor.behavior = "panXY";
                chart.cursor.xAxis = dateAxis;
                chart.cursor.snapToSeries = series;

// Create vertical scrollbar and place it before the value axis
                chart.scrollbarY = new am4core.Scrollbar();
                chart.scrollbarY.parent = chart.leftAxesContainer;
                chart.scrollbarY.toBack();

// Create a horizontal scrollbar with previe and place it underneath the date axis
                chart.scrollbarX = new am4charts.XYChartScrollbar();
                chart.scrollbarX.series.push(series);
                chart.scrollbarX.parent = chart.bottomAxesContainer;

                dateAxis.keepSelection = true;
                chart.language.locale = am4lang_vi_VN;
                chart.dateFormatter.language = new am4core.Language();
                chart.dateFormatter.language.locale = am4lang_vi_VN;

            }); // end am4core.ready()
        }
    </script>
    <!-- HTML -->

@endsection
@endsection
