@extends('layouts.app')

@section('content')
    <div class="tab-content">
        {{--        tab leave --}}
        <div
            class="tab-pane leave {{!request()->attendances && !isset(request()->attendance_search)  || request()->leave_search ? 'active' : '' }}"
            id="leave"
            role="tabpanel">
            {{-- search form--}}
            <div class="card flex-fill">
                <div class="card-body row">
                    <div class="col-md-3 m10b">
                        <form name="form_leave_search" id="form_leave_search" action="{{route('manage_leave')}}"
                              method="get">
                            <input type="text" class="form-control" name="leave_search"
                                   value="{{ request()->leave_search }}"
                                   id="leave_search" autocomplete="leave_search" placeholder="Nội dung...">
                        </form>
                    </div>
                    <div class="col-md-5 m10b">
                        <button class="btn btn-success" id="button_leave_search" name=button_search" type="submit"
                                form="form_leave_search"
                                aria-expanded="false">
                            <i class="fa fa-search"> </i> Tìm kiếm
                        </button>
                    </div>
                    {{--                    <div class="col-md-4 text-left text-md-right">--}}
                    {{--                        <div class="list-inline-item">--}}
                    {{--                            <a href="#" class="btn btn-primary" data-toggle="dropdown" aria-expanded="false">--}}
                    {{--                                <i class="fa fa-file"></i>--}}
                    {{--                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"--}}
                    {{--                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"--}}
                    {{--                                     stroke-linejoin="round" class="feather feather-more-horizontal">--}}
                    {{--                                    <circle cx="12" cy="12" r="1"></circle>--}}
                    {{--                                    <circle cx="19" cy="12" r="1"></circle>--}}
                    {{--                                    <circle cx="5" cy="12" r="1"></circle>--}}
                    {{--                                </svg>--}}
                    {{--                            </a>--}}
                    {{--                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end"--}}
                    {{--                                 style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(1711px, 53px, 0px);">--}}
                    {{--                                <a href="#" class="dropdown-item"><i class="iconsminds-file"></i> Download CSV</a>--}}
                    {{--                                <a href="#" class="dropdown-item"><i class="iconsminds-photo"></i> Download Image</a>--}}
                    {{--                                <a href="#" class="dropdown-item"><i class="iconsminds-file-copy"></i> Copy Image</a>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
            </div>
            {{--    leave table--}}
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive table-user">
                        @if(!empty($leaves))
                            {!! $leaves->appends(Arr::except(request()->all(), ['attendances', 'attendance_search']))->links('layouts.paginator') !!}
                        @endif
                        <form action="" id="form_leave" method="post">
                            @csrf
                            <div class="table-responsive">
                                <table class="table" id="table_leave">
                                    <thead>
                                    <tr>
                                        <th scope="col" style="width: 20px"><input type="checkbox"
                                                                                   id="check_all_leave"
                                                                                   class="action_check_leave"/></th>
                                        <th scope="col" style="width: 30px">#ID</th>
                                        <th scope="col" style="width: 50px">UserName</th>
                                        <th scope="col">Email</th>
                                        <th style="min-width: 180px" scope="col">Số điện thoại</th>
                                        <th scope="col">Ngày Nghỉ</th>
                                        <th scope="col">Nhóm</th>
                                        <th scope="col">Lý do</th>
                                        <th class="text-center px-0" scope="col" style="width: 300px">Thao tác</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($leaves as $leave)

                                        <tr>
                                            <th><input class="action_check_leave" type="checkbox" name="id_leave[]"
                                                       value="{{$leave->id}}"/></th>
                                            <th scope="row">{{$leave->id}}</th>
                                            <td>{{$leave->user->username}}</td>
                                            <td>{{$leave->user->email}}</td>
                                            <td>
                                                <div class="hiden-td-table">{{$leave->user->phone_number}}</div>
                                            </td>
                                            <td>
                                                <div class="hiden-td-table">{{$leave->date_leave}}</div>
                                            </td>
                                            <td>
                                                <div class="hiden-td-table">{{$leave->leave_group}}</div>
                                            </td>
                                            <td>
                                                <div class="hiden-td-table">{{$leave->content}}</div>
                                            </td>
                                            <td class="text-center px-0">
                                                <div style="width: 200px">
                                                    @if(!$leave->status)
                                                        @if($leave->leave_group)
                                                            <a class="btn btn-primary"
                                                               href="{{route('yes_leave_group', $leave->leave_group)}}"
                                                               style="border-left: none"
                                                            >Duyệt nhóm</a>
                                                        @endif
                                                        <a class="btn btn-success"
                                                           href="{{route('yes_leave', $leave->id)}}"
                                                           style="border-left: none"
                                                        >Duyệt</a>
                                                    @else
                                                        @if($leave->leave_group)
                                                            <a class="btn btn-primary"
                                                               href="{{route('no_leave_group', $leave->leave_group)}}"
                                                               style="border-left: none"
                                                            >Hủy duyệt nhóm</a>
                                                        @endif
                                                        <a class="btn btn-danger"
                                                           href="{{route('no_leave', $leave->id)}}"
                                                           style="border-left: none"
                                                        >Hủy duyệt</a>
                                                </div>
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="9" class="text-center text-secondary">Không có dữ liệu!</td>
                                        </tr>
                                    @endforelse

                                    </tbody>
                                </table>
                            </div>
                        </form>
                        <div id="action_leave" class="pt-2"><a href="{{route('yes_multiple_leave')}}"
                                                               id="yes_multiple_leave"
                                                               class="btn btn-primary btn_action_leave_check disabled">Duyệt</a>
                            @if(!empty($leaves))
                                {!! $leaves->appends(Arr::except(request()->all(), ['attendances', 'attendance_search']))->links('layouts.paginator') !!}
                            @endif
                        </div>
                    </div>
                </div>


            </div>
            @section('after_js')
                <script language="javascript">
                    $(document).ready(function () {
                        $("#table_leave #check_all_leave").click(function () {
                            if ($("#table_leave #check_all_leave").is(':checked')) {
                                $("#table_leave input[type=checkbox]").each(function () {
                                    $(this).prop("checked", true);
                                });

                            } else {
                                $("#table_leave input[type=checkbox]").each(function () {
                                    $(this).prop("checked", false);
                                });
                            }
                        });

                        $('.action_check_leave').click(function () {
                            let check_leave = false;
                            $("#table_leave input[type=checkbox]").each(function () {
                                if ($(this).is(":checked")) {
                                    check_leave = true;
                                    return false;
                                }
                            });

                            if (check_leave) {
                                $('.btn_action_leave_check').each(function () {
                                    $(this).removeClass("disabled");
                                });
                            } else {
                                $('.btn_action_leave_check').each(function () {
                                    $(this).addClass("disabled");
                                });
                            }

                        });

                        $('.btn_action_leave_check').click(function (e) {
                            e.preventDefault();
                            let url_action = $(this).attr('href');
                            $('#form_leave').attr('action', url_action).submit();
                        });
                    });
                </script>
@endsection
@endsection
