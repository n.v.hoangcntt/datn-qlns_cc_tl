@extends('layouts.app')

@section('content')
    <div class="pb-4">
        <h2>Chấm công
            <div class="float-right">
                @if(checkQuanly())
                    <a class="btn btn-primary" style="padding: 5px 5px; font-size: 12px"
                       href="{{route('manage_attendance')}}">Quản
                        lý công nhân viên</a>
                @endif
            </div>
        </h2>
    </div>
    <div class="row card-group">
        <div class="col-12 d-flex">
            <div class="card flex-fill">
                {{--Form xin nghỉ--}}
                <div class="card-body" style="min-height: 88vh">
                    <div class="card">
                        <div class="card-body d-flex justify-content-md-around">
                            <a href="{{route('checkin')}}"
                               class="btn btn-primary {{$checkAttendance && $checkAttendance->check_in && $checkAttendance->check_in != null ? 'disabled': '' }}">
                                Checkin
                            </a>
                            <a href="{{route('checkout')}}"
                               class="btn btn-primary {{$checkAttendance && $checkAttendance->check_out && $checkAttendance->check_out != null ? 'disabled': '' }}">Checkout
                            </a>
                        </div>
                    </div>

{{--                    <div class="card flex-fill">--}}
{{--                        <div class="card-body row">--}}
{{--                            <div class="col-md-6 m10b">--}}
{{--                                <form name="form_search" class="form-row" id="form_leave_search"--}}
{{--                                      action="{{route('leave')}}"--}}
{{--                                      method="get">--}}
{{--                                    <input type="text" class="form-control col-md-7 ml-5" name="leave_search"--}}
{{--                                           value="{{ request()->leave_search }}"--}}
{{--                                           id="leave_search" autocomplete="leave_search" placeholder="nội dung ...">--}}
{{--                                    <button type="submit" class="btn btn-success col-md-3 offset-1" id="button_search"--}}
{{--                                            name=button_search" type="button"--}}
{{--                                            aria-expanded="false">--}}
{{--                                        <i class="fa fa-search"> </i> Tìm--}}
{{--                                    </button>--}}
{{--                                </form>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    {{--    leave table--}}
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive table-user">
                                @if(!empty($attendanceUser))
                                    {!! $attendanceUser->appends(request()->all())->links('layouts.paginator') !!}
                                @endif
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col" style="width: 5%">#ID</th>
                                        <th scope="col">Trạng thái</th>
                                        <th scope="col">Ngày</th>
                                        <th scope="col" style="width: 15%">Giờ vào</th>
                                        <th scope="col" style="width: 12%">Vào muộn</th>
                                        <th style="width: 15%" scope="col">Giờ ra</th>

                                        <th scope="col" style="width: 12%">Tăng ca</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($attendanceUser as $attendance)

                                        <tr>
                                            <th scope="row">{{$attendance->id}}</th>
                                            <th scope="row"></th>
                                            <td>{{$attendance->attendances_date}}</td>
                                            <td> {{$attendance->check_in ? \Carbon\Carbon::create($attendance->check_in)->format('H:i') : ''}}</td>
                                            <th scope="row">{{$attendance->late_time  && $attendance->late_time > 0 ? $attendance->late_time . ' phút': ''}}</th>
                                            <td>{{$attendance->check_out ? \Carbon\Carbon::create($attendance->check_out)->format('H:i') : ''}}</td>
                                            <td>{{$attendance->over_time ? $attendance->over_time.' phút': '0'}}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6" class="text-center text-secondary">Không có dữ liệu!</td>
                                        </tr>
                                    @endforelse

                                    </tbody>
                                </table>
                                @if(!empty($attendanceUser))
                                    {!! $attendanceUser->appends(request()->all())->links('layouts.paginator') !!}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end card-->
    </div>

    @include('user.modal_edit_leave')
@endsection
