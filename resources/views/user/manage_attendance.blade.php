@extends('layouts.app')

@section('content')
    <div class="tab-pane attendance {{request()->attendances || request()->attendance_search ? 'active' : '' }}"
         id="attendance"
         role="tabpanel">

        {{-- search form--}}
        <div class="card flex-fill">
            <div class="card-body row">
                <div class="col-md-3 m10b">
                    <form name="form_attendance_search" id="form_attendance_search"
                          action="{{route('manage_attendance')}}"
                          method="get">
                        <input type="text" class="form-control" name="attendance_search"
                               value="{{ request()->attendance_search }}"
                               id="attendance_search" autocomplete="attendance_search" placeholder="Nội dung...">
                    </form>
                </div>
                <div class="col-md-5 m10b">
                    <button class="btn btn-success" id="button_attendance_search" name=button_attendance_search"
                            type="submit" form="form_attendance_search"
                            aria-expanded="false">
                        <i class="fa fa-search"> </i> Tìm kiếm
                    </button>
                </div>
                {{--                    <div class="col-md-4 text-left text-md-right">--}}
                {{--                        <div class="list-inline-item">--}}
                {{--                            <a href="#" class="btn btn-primary" data-toggle="dropdown" aria-expanded="false">--}}
                {{--                                <i class="fa fa-file"></i>--}}
                {{--                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"--}}
                {{--                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"--}}
                {{--                                     stroke-linejoin="round" class="feather feather-more-horizontal">--}}
                {{--                                    <circle cx="12" cy="12" r="1"></circle>--}}
                {{--                                    <circle cx="19" cy="12" r="1"></circle>--}}
                {{--                                    <circle cx="5" cy="12" r="1"></circle>--}}
                {{--                                </svg>--}}
                {{--                            </a>--}}
                {{--                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end"--}}
                {{--                                 style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(1711px, 53px, 0px);">--}}
                {{--                                <a href="#" class="dropdown-item"><i class="iconsminds-file"></i> Download CSV</a>--}}
                {{--                                <a href="#" class="dropdown-item"><i class="iconsminds-photo"></i> Download Image</a>--}}
                {{--                                <a href="#" class="dropdown-item"><i class="iconsminds-file-copy"></i> Copy Image</a>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
            </div>
        </div>

        {{--    attendance table--}}
        <div class="card">
            <div class="card-body">
                <div class="table-responsive table-user">
                    @if(!empty($attendances))
                        {!! $attendances->appends(Arr::except(request()->all(), ['leaves', 'leave_search']))->links('layouts.paginator') !!}
                    @endif
                    <form action="" id="form_attendance" method="post">
                        @csrf
                        <table class="table" id="table_attendance">
                            <thead>
                            <tr>
                                <th scope="col" style="width: 20px">
                                    <input type="checkbox" id="check_all_attendance"
                                           class="action_check_attendance"/>
                                </th>
                                <th scope="col" style="width: 5%">#ID</th>
                                <th scope="col" >UserName</th>
                                <th scope="col" class="text-center" >Ngày</th>
                                <th style="width: 15%" class="text-center" scope="col">Giờ vào - Giờ ra</th>
                                <th scope="col" >Tăng ca</th>
                                <th class="text-center" scope="col">Xác nhận tăng ca</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($attendances as $attendance)
                                <tr>
                                    <td><input type="checkbox" class="check_attendance action_check_attendance"
                                               name="id_attendance[]" value="{{$attendance->id}}"/>
                                    </td>
                                    <th scope="row">{{$attendance->id}}</th>
                                    <td>{{$attendance->user['username']}}</td>
                                    <td class="text-center">{{$attendance->attendances_date}}</td>
                                    <td class="text-center">{{$attendance->check_in ? \Carbon\Carbon::create($attendance->check_in)->format('H:i') : ''}}
                                        - {{$attendance->check_out ? \Carbon\Carbon::create($attendance->check_out)->format('H:i') : ''}}</td>
                                    <td>{{$attendance->over_time}}</td>
                                    <td class="text-center">
                                        @if($attendance->status == 1)
                                            <a class="btn btn-warning "
                                               href="{{route('no_over_time',$attendance->id )}}"
                                            >Hủy bỏ</a>
                                        @elseif($attendance->status == 0)
                                            <a class="btn btn-warning "
                                               href="{{route('yes_over_time', $attendance->id)}}"
                                            >Xác nhận</a>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7" class="text-center text-secondary">Không có dữ liệu!</td>
                                </tr>
                            @endforelse

                            </tbody>
                        </table>
                    </form>
                    <div id="action_attendance" class="pt-2">
                        <a href="{{route('yes_multiple_attendance')}}"
                           id="yes_multiple_attendance"
                           class="btn btn-primary btn_action_attendance_check disabled">Xác nhận
                        </a>
                    </div>
                    @if(!empty($attendances))
                        {!! $attendances->appends(Arr::except(request()->all(), ['leaves', 'leave_search']))->links('layouts.paginator') !!}
                    @endif
                </div>
            </div>
        </div>

    </div>

@section('after_js')
    <script language="javascript">
        $(document).ready(function () {
            $("#table_attendance #check_all_attendance").click(function () {
                if ($("#table_attendance #check_all_attendance").is(':checked')) {
                    $("#table_attendance input[type=checkbox]").each(function () {
                        $(this).prop("checked", true);
                    });

                } else {
                    $("#table_attendance input[type=checkbox]").each(function () {
                        $(this).prop("checked", false);
                    });
                }
            });

            $('.action_check_attendance').click(function () {
                let check_attendance = false;
                $("#table_attendance input[type=checkbox]").each(function () {
                    if ($(this).is(":checked")) {
                        check_attendance = true;
                        return false;
                    }
                });

                if (check_attendance) {
                    $('.btn_action_attendance_check').each(function () {
                        $(this).removeClass("disabled");
                    });
                } else {
                    $('.btn_action_attendance_check').each(function () {
                        $(this).addClass("disabled");
                    });
                }

            });

            $('.btn_action_attendance_check').click(function (e) {
                e.preventDefault();
                let url_action = $(this).attr('href');
                $('#form_attendance').attr('action', url_action).submit();
            });
        });
    </script>
@endsection
@endsection
