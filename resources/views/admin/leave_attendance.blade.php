@extends('layouts.layout_admin')


@section('title')
    {{trans('layout.Leave & Attendance Management')}}
@endsection


@section('title_page')
    {{trans('layout.Leave & Attendance Management')}}
@endsection
@section('section_header')

@endsection
@section('before_js')


@endsection
@section('content')

    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link {{!request()->attendances && !isset(request()->attendance_search) || request()->leave_search ? 'active' : '' }}"
               data-toggle="tab" href="#leave" role="tab"><i
                    class="fa fa-home"></i>
                Nghỉ phép</a>
        </li>

        <li class="nav-item">
            <a class="nav-link {{request()->attendances || request()->attendance_search ? 'active' : '' }}"
               data-toggle="tab" href="#attendance"
               role="tab"><i class="iconsminds-finger-print"></i> Chấm Công</a>
        </li>
    </ul>


    <!-- Tab panes -->
    <div class="tab-content">
        {{--        tab leave --}}
        <div
            class="tab-pane leave {{!request()->attendances && !isset(request()->attendance_search)  || request()->leave_search ? 'active' : '' }}"
            id="leave"
            role="tabpanel">
            {{-- search form--}}
            <div class="card flex-fill">
                <div class="card-body row">
                    <div class="col-md-3 m10b">
                        <form name="form_leave_search" id="form_leave_search" action="{{route('leave_attendance')}}"
                              method="get">
                            <input type="text" class="form-control" name="leave_search"
                                   value="{{ request()->leave_search }}"
                                   id="leave_search" autocomplete="leave_search" placeholder="Nội dung...">
                        </form>
                    </div>
                    <div class="col-md-5 m10b">
                        <button class="btn btn-success" id="button_leave_search" name=button_search" type="button"
                                aria-expanded="false">
                            <i class="fa fa-search"> </i> Tìm kiếm
                        </button>
                    </div>
{{--                    <div class="col-md-4 text-left text-md-right">--}}
{{--                        <div class="list-inline-item">--}}
{{--                            <a href="#" class="btn btn-primary" data-toggle="dropdown" aria-expanded="false">--}}
{{--                                <i class="fa fa-file"></i>--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"--}}
{{--                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"--}}
{{--                                     stroke-linejoin="round" class="feather feather-more-horizontal">--}}
{{--                                    <circle cx="12" cy="12" r="1"></circle>--}}
{{--                                    <circle cx="19" cy="12" r="1"></circle>--}}
{{--                                    <circle cx="5" cy="12" r="1"></circle>--}}
{{--                                </svg>--}}
{{--                            </a>--}}
{{--                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end"--}}
{{--                                 style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(1711px, 53px, 0px);">--}}
{{--                                <a href="#" class="dropdown-item"><i class="iconsminds-file"></i> Download CSV</a>--}}
{{--                                <a href="#" class="dropdown-item"><i class="iconsminds-photo"></i> Download Image</a>--}}
{{--                                <a href="#" class="dropdown-item"><i class="iconsminds-file-copy"></i> Copy Image</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
            {{--    leave table--}}
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive table-user">
                        @if(!empty($leaves))
                            {!! $leaves->appends(Arr::except(request()->all(), ['attendances', 'attendance_search']))->links('layouts.paginator') !!}
                        @endif
                        <form action="" id="form_leave" method="post">
                            @csrf
                            <div class="table-responsive">
                                <table class="table" id="table_leave">
                                    <thead>
                                    <tr>
                                        <th scope="col" style="width: 20px"><input type="checkbox"
                                                                                   id="check_all_leave" class="action_check_leave"/></th>
                                        <th scope="col" style="width: 30px">#ID</th>
                                        <th scope="col" style="width: 50px">UserName</th>
                                        <th scope="col">Email</th>
                                        <th style="min-width: 180px" scope="col">Số điện thoại</th>
                                        <th scope="col">Ngày Nghỉ</th>
                                        <th scope="col">Nhóm</th>
                                        <th scope="col">Lý do</th>
                                        <th class="text-center px-0" scope="col" style="width: 300px">Thao tác</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($leaves as $leave)

                                        <tr>
                                            <th><input class="action_check_leave" type="checkbox" name="id_leave[]" value="{{$leave->id}}"/></th>
                                            <th scope="row">{{$leave->id}}</th>
                                            <td>{{$leave->user->username}}</td>
                                            <td>{{$leave->user->email}}</td>
                                            <td><div class="hiden-td-table">{{$leave->user->phone_number}}</div></td>
                                            <td><div class="hiden-td-table">{{$leave->date_leave}}</div></td>
                                            <td>
                                                <div class="hiden-td-table">{{$leave->leave_group}}</div></td>
                                            <td> <div class="hiden-td-table">{{$leave->content}}</div></td>
                                            <td class="text-center px-0">
                                                <div style="width: 200px">
                                                @if(!$leave->status)
                                                    @if($leave->leave_group)
                                                        <a class="btn btn-primary"
                                                           href="{{route('yes_leave_group', $leave->leave_group)}}"
                                                           style="border-left: none"
                                                        >Duyệt nhóm</a>
                                                    @endif
                                                    <a class="btn btn-success"
                                                       href="{{route('yes_leave', $leave->id)}}"
                                                       style="border-left: none"
                                                    >Duyệt</a>
                                                    <a class="btn btn-outline-danger text-delete btn_confirmed_delete"
                                                       data-id="{{$leave->id}}" data-target="#modalComfirmedDelete"
                                                       href="{{route('leaveDelete', $leave->id)}}">Xóa</a>
                                                @else
                                                    @if($leave->leave_group)
                                                        <a class="btn btn-primary"
                                                           href="{{route('no_leave_group', $leave->leave_group)}}"
                                                           style="border-left: none"
                                                        >Hủy duyệt nhóm</a>
                                                    @endif
                                                    <a class="btn btn-danger"
                                                       href="{{route('no_leave', $leave->id)}}"
                                                       style="border-left: none"
                                                    >Hủy duyệt</a>
                                                </div>
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="9" class="text-center text-secondary">Không có dữ liệu!</td>
                                        </tr>
                                    @endforelse

                                    </tbody>
                                </table>
                            </div>
                        </form>
                            <div id="action_leave" class="pt-2"><a href="{{route('yes_multiple_leave')}}" id="yes_multiple_leave" class="btn btn-primary btn_action_leave_check disabled">Duyệt</a> <a href="{{route('delete_multiple_leave')}}" class="btn btn-danger btn_action_leave_check disabled">Xóa</a></div>
                        @if(!empty($leaves))
                            {!! $leaves->appends(Arr::except(request()->all(), ['attendances', 'attendance_search']))->links('layouts.paginator') !!}
                        @endif
                    </div>
                </div>
            </div>


        </div>

        {{--end tab leave--}}


        {{--            tab attendance --}}
        <div class="tab-pane attendance {{request()->attendances || request()->attendance_search ? 'active' : '' }}"
             id="attendance"
             role="tabpanel">

            {{-- search form--}}
            <div class="card flex-fill">
                <div class="card-body row">
                    <div class="col-md-3 m10b">
                        <form name="form_attendance_search" id="form_attendance_search"
                              action="{{route('leave_attendance')}}"
                              method="get">
                            <input type="text" class="form-control" name="attendance_search"
                                   value="{{ request()->attendance_search }}"
                                   id="attendance_search" autocomplete="attendance_search" placeholder="Nội dung...">
                        </form>
                    </div>
                    <div class="col-md-5 m10b">
                        <button class="btn btn-success" id="button_attendance_search" name=button_attendance_search"
                                type="button"
                                aria-expanded="false">
                            <i class="fa fa-search"> </i> Tìm kiếm
                        </button>
                    </div>
                    <div class="col-md-4 text-left text-md-right">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#import_file_attendance_modal">
                            Import
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="import_file_attendance_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Nhập dữ liệu chấm công</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form name="import_attendance_form" id="import_attendance_form" method="post" enctype="multipart/form-data" action="{{route('import_attendance')}}">
                                            @csrf
                                            <div class="form-group row px-3">
                                                <label for="import_file">Chọn file</label>
                                                <input type="file"  style="border: 1px solid #ced4da; border-radius: .25rem; width: 100%" name="import_file" required accept=".xlsx,.xls,.csv">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer d-flex justify-content-end">
                                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Hủy</button>
                                        <button type="submit" form="import_attendance_form" class="btn btn-primary">Import</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="col-md-4 text-left text-md-right">--}}
{{--                        <div class="list-inline-item">--}}
{{--                            <a href="#" class="btn btn-primary" data-toggle="dropdown" aria-expanded="false">--}}
{{--                                <i class="fa fa-file"></i>--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"--}}
{{--                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"--}}
{{--                                     stroke-linejoin="round" class="feather feather-more-horizontal">--}}
{{--                                    <circle cx="12" cy="12" r="1"></circle>--}}
{{--                                    <circle cx="19" cy="12" r="1"></circle>--}}
{{--                                    <circle cx="5" cy="12" r="1"></circle>--}}
{{--                                </svg>--}}
{{--                            </a>--}}
{{--                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end"--}}
{{--                                 style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(1711px, 53px, 0px);">--}}
{{--                                <a href="#" class="dropdown-item"><i class="iconsminds-file"></i> Download CSV</a>--}}
{{--                                <a href="#" class="dropdown-item"><i class="iconsminds-photo"></i> Download Image</a>--}}
{{--                                <a href="#" class="dropdown-item"><i class="iconsminds-file-copy"></i> Copy Image</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>

            {{--    attendance table--}}
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive table-user">
                        @if(!empty($attendances))
                            {!! $attendances->appends(Arr::except(request()->all(), ['leaves', 'leave_search']))->links('layouts.paginator') !!}
                        @endif
                            <form action="" id="form_attendance" method="post">
                                @csrf
                                <table class="table" id="table_attendance">
                                    <thead>
                                    <tr>
                                        <th scope="col">
                                            <input type="checkbox" id="check_all_attendance"
                                                   class="action_check_attendance"/>
                                        </th>
                                        <th scope="col" style="width: 5%">#ID</th>
                                        <th scope="col" style="width: 12%">UserName</th>
                                        <th scope="col" class="text-center" style="width: 15%">Ngày</th>
                                        <th style="width: 15%" class="text-center" scope="col">Giờ vào - Giờ ra</th>
                                        <th scope="col" style="width: 10%">Tăng ca</th>
                                        <th class="text-center" scope="col">Xác nhận tăng ca</th>
                                        <th class="text-center" scope="col" style="width: 15%">Thao tác</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($attendances as $attendance)
                                        <tr>
                                            <td><input type="checkbox" class="check_attendance action_check_attendance"
                                                       name="id_attendance[]" value="{{$attendance->id}}"/>
                                            </td>
                                            <th scope="row">{{$attendance->id}}</th>
                                            <td>{{$attendance->user['username']}}</td>
                                            <td class="text-center">{{$attendance->attendances_date}}</td>
                                            <td class="text-center">{{$attendance->check_in ? \Carbon\Carbon::create($attendance->check_in)->format('H:i') : ''}}
                                                - {{$attendance->check_out ? \Carbon\Carbon::create($attendance->check_out)->format('H:i') : ''}}</td>
                                            <td>{{$attendance->over_time}}</td>
                                            <td class="text-center">
                                                @if($attendance->status == 1)
                                                    <a class="btn btn-warning "
                                                       href="{{route('no_over_time',$attendance->id )}}"
                                                    >Hủy bỏ</a>
                                                @elseif($attendance->status == 0)
                                                    <a class="btn btn-warning " href="{{route('yes_over_time', $attendance->id)}}"
                                                    >Xác nhận</a>
                                                @endif
                                            </td>
                                            <td class="d-flex justify-content-around">
                                                <a class="btn btn-success btn_edit_attendance mr-1" data-data="{{$attendance}}" data-id="{{$attendance->id}}"
                                                   data-toggle="modal"
                                                   data-target="#edit_attendance_modal">Sửa</a>
                                                <a class="btn btn-outline-danger text-delete btn_confirmed_delete"
                                                   data-id="{{$attendance->id}}" data-target="#modalComfirmedDelete"
                                                   href="{{route('deleteAttendance', $attendance->id)}}">Xóa</a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7" class="text-center text-secondary">Không có dữ liệu!</td>
                                        </tr>
                                    @endforelse

                                    </tbody>
                                </table>
                            </form>
                            <div id="action_attendance" class="pt-2"><a href="{{route('yes_multiple_attendance')}}"
                                                                        id="yes_multiple_attendance"
                                                                        class="btn btn-primary btn_action_attendance_check disabled">Xác
                                    nhận</a> <a href="{{route('delete_multiple_attendance')}}"
                                                class="btn btn-danger btn_action_attendance_check disabled">Xóa</a>
                            </div>
                            @if(!empty($attendances))
                                {!! $attendances->appends(Arr::except(request()->all(), ['leaves', 'leave_search']))->links('layouts.paginator') !!}
                            @endif
                    </div>
                </div>
            </div>

        </div>

    </div>

{{--    modal sửa chấm công--}}
    <div class="modal fade" id="edit_attendance_modal" tabindex="-1" role="dialog"
         aria-labelledby="edit_attendance_modal"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel"></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="attendance_update_modal" method="post" action="{{route('update_attendance')}}">
                        @csrf
                        <input type="text" name="id" id="attendance_id" hidden>
                        <h3 id="text_username" ></h3>
                        <div class="form-group row">
                            <label for="attendances_date" class="col-sm-3 col-form-label">Ngày</label>
                            <div class="col-sm-9">
                                <input id="attendances_date" name="attendances_date" type="text" class="form-control" readonly required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="time_check_in" class="col-sm-3 col-form-label">Giờ vào</label>
                            <div class="col-sm-9">
                                <input id="time_check_in" name="time_check_in" type="text" class="form-control" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="time_check_out" class="col-sm-3 col-form-label">Giờ Ra</label>
                            <div class="col-sm-9">
                                <input id="time_check_out" name="time_check_out" type="text" class="form-control" >
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer d-flex justify-content-around">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary" form="attendance_update_modal">Cập nhập</button>
                </div>
            </div>
        </div>
    </div>


@section('after_js')
    <script src="{{asset('js/admin/leave_attendance.js')}}" defer></script>
@endsection

@endsection
