@extends('layouts.layout_admin')

@section('title')
    {{trans('layout.User Management')}}
@endsection


@section('title_page')
    {{trans('layout.User Management')}}
@endsection
@section('section_header')
    <a href="#" class="btn btn-primary btn-add btn_add_user" data-toggle="modal" data-target="#formModalCreateUser"><i
            class="iconsminds-add-user"></i> Add user</a>
@endsection
@section('before_js')
    <script type="text/javascript"
            src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js" defer></script>

@endsection
@section('content')

    <div class="filter-wrapper">
        <div class="card flex-fill">
            <div class="card-body row">
                <div class="col-md-3 m10b">
                    <form name="form_search" id="form_search" action="{{route("user_management.index")}}" method="get">
                        <input type="text" class="form-control" name="key_search" value="{{ request()->key_search }}"
                               id="key_search" autocomplete="key_search" placeholder="Tên/Email/Số điện thoại...">
                    </form>
                </div>
                <div class="col-md-5 m10b">
                    <button class="btn btn-success" id="button_search" name=button_search" type="button"
                            aria-expanded="false">
                        <i class="fa fa-search"> </i> Tìm kiếm
                    </button>
                </div>
{{--                <div class="col-md-4 text-left text-md-right">--}}
{{--                    <div class="list-inline-item">--}}
{{--                        <a href="#" class="btn btn-primary" data-toggle="dropdown" aria-expanded="false">--}}
{{--                            <i class="fa fa-file"></i>--}}
{{--                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"--}}
{{--                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"--}}
{{--                                 stroke-linejoin="round" class="feather feather-more-horizontal">--}}
{{--                                <circle cx="12" cy="12" r="1"></circle>--}}
{{--                                <circle cx="19" cy="12" r="1"></circle>--}}
{{--                                <circle cx="5" cy="12" r="1"></circle>--}}
{{--                            </svg>--}}
{{--                        </a>--}}
{{--                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end"--}}
{{--                             style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(1711px, 53px, 0px);">--}}
{{--                            <a href="#" class="dropdown-item"><i class="iconsminds-file"></i> Download CSV</a>--}}
{{--                            <a href="#" class="dropdown-item"><i class="iconsminds-photo"></i> Download Image</a>--}}
{{--                            <a href="#" class="dropdown-item"><i class="iconsminds-file-copy"></i> Copy Image</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="table-responsive table-user">
                @if(!empty($listUser))
                    {!! $listUser->appends(request()->all())->links('layouts.paginator') !!}
                @endif
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#ID</th>
                        <th scope="col">Họ và tên</th>
                        <th scope="col">Email</th>
                        <th class="text-center" scope="col">Số điện thoại</th>
                        <th scope="col">Vị trí</th>
                        <th class="text-center" scope="col">Thao tác</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($listUser as $user)

                        <tr>
                            <th scope="row">{{$user->id}}</th>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td class="text-center">{{$user->phone_number}}</td>
                            <td>{{$user->role->role_name}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button
                                        class="btn btn-secondary btn-sm btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split cast"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Thao tác <span
                                            class="sr-only border-left">Action Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item edit_user" href="#" data-id="{{$user->id}}"
                                           data-data="{{$user}}" data-toggle="modal"
                                           data-target="#fromModalUpdate">Sửa</a>
                                        <div class="dropdown-divider my-0"></div>
                                        <a class="dropdown-item text-delete btn_delete" data-toggle="modal"
                                           data-id="{{$user->id}}" data-target="#confirmModal" href="#">Xóa</a>
                                        <div class="dropdown-divider my-0"></div>
                                        @if(empty($user->contract))
                                            <a class="dropdown-item text-success btn_add_contract" href="#" data-toggle="modal"
                                               data-user_id="{{$user->id}}" data-target="#ThemHopDongModal">Thêm hợp đồng</a>
                                        @else
                                            <a class="dropdown-item text-success btn_edit_contract" href="#" data-toggle="modal"
                                               data-user_id="{{$user->id}}" data-contract="{{$user->contract}}" data-target="#ThemHopDongModal" >Sửa hợp đồng</a>
                                        @endif
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6" class="text-center text-secondary">Không có dữ liệu!</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
                @if(!empty($listUser))
                    {!! $listUser->appends(request()->all())->links('layouts.paginator') !!}
                @endif
            </div>
        </div>
    </div>
    <!--end card-->
{{--    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#view_file">--}}
{{--        Launch demo modal--}}
{{--    </button>--}}

    @include('modal.create_user')
    @include('modal.confirm_delete_user')
    @include('modal.them_hop_dong')
@section('after_js')
    <script src="{{asset('js/admin/user_management.js')}}" defer></script>
@endsection

@endsection
