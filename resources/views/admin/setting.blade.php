@extends('layouts.layout_admin')


@section('title')
    {{trans('layout.Setting')}}
@endsection


@section('title_page')
    {{trans('layout.Setting')}}
@endsection
@section('section_header')

@endsection
@section('before_js')


@endsection
@section('content')

    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tabs-account" role="tab"><i class="iconsminds-male-female"></i>
                Tài khoản</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link {{empty($changePassword) ? '' : 'active'}}" data-toggle="tab" href="#tabs-security"
               role="tab"><i class="iconsminds-security-settings"></i> Đổi mật khẩu</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{empty($changePassword) ? 'active' : ''}}" data-toggle="tab" href="#tabs-setting-system"
               role="tab"><i class="iconsminds-gears"></i> Cài đặt hệ thống</a>
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane tabs-account" id="tabs-account" role="tabpanel">
            <form class="row" method="post" action="{{route('updateInfor')}}">
                @csrf
                <div class="form-group col-md-4">
                    <label for="Email">Email</label>
                    <input type="email" class="form-control" id="Email" aria-describedby="emailHelp"
                           placeholder="Enter email" value="{{auth()->user()->email}}">
                    <small id="emailHelp" class="form-text text-muted">Bạn không nên chia sẻ email cho người
                        khác</small>
                </div>
                <div class="form-group col-md-4">
                    <label for="Name">Tên</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter name"
                           value="{{auth()->user()->name}}">
                </div>
                <div class="form-group col-md-4">
                    <label for="Phone">Số điện thoại</label>
                    <input type="text" class="form-control" name='phone_number' id="phone_number"
                           value="{{auth()->user()->phone_number}}" placeholder="Nhập số điện thoại">
                </div>
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-primary btn-save"><i class="iconsminds-save"></i> Lưu</button>
                </div>
            </form>
        </div>
        <!--end tabs-account-->
        <div class="tab-pane tabs-security {{empty($changePassword) ? '' : 'active'}}" id="tabs-security"
             role="tabpanel">
            <form class="row" action="{{route('storePassword')}}" method="post">
                @csrf
                <div class="form-group col-md-4">
                    <label for="oldPassword">Mật khẩu cũ *</label>
                    <input type="password" class="form-control" id="oldPassword" name='oldPassword'
                           placeholder="Mật khẩu">
                </div>
                <div class="form-group col-md-4">
                    <label for="newPassword">Mật khẩu mới *</label>
                    <input type="password" class="form-control" id="newPassword" name='password'
                           placeholder="Mật khẩu mới">
                </div>
                <div class="form-group col-md-4">
                    <label for="retypePassword">Nhập lại mật khẩu *</label>
                    <input type="password" class="form-control" id="retypePassword" name="password_confirmation"
                           placeholder="Nhập lại mật khẩu">
                </div>
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-primary btn-save"><i class="iconsminds-save"></i> Lưu</button>
                </div>
            </form>
        </div>


        {{--            tab setting system --}}
        <div class="tab-pane tabs-setting-system {{empty($changePassword) ? 'active' : ''}}" id="tabs-setting-system"
             role="tabpanel">
            <form class="row" action="{{route('storeSetting')}}" method="POST">
                @csrf
                <div class="form-group col-md-4">
                    <label for="start_time">Giờ vào:</label>
                    <input type="time" class="form-control" name="start_time" id="start_time"
                           value="{{$setting->start_time ?? '08:30' }}" placeholder="Giờ vào">
                </div>
                <div class="form-group col-md-4">
                    <label for="end_time">Giờ ra:</label>
                    <input type="time" class="form-control" name="end_time" id="end_time"
                           value="{{$setting->end_time ?? '17:30' }}" placeholder="Giờ ra">
                </div>
                <div class="form-group col-md-12">
                    <button type="submit" class="btn btn-primary "><i class="iconsminds-save"></i> Lưu</button>
                </div>
            </form>

            <div>
                <div class="form-group col-md-12">
                    <label>Cho phép chấm công Online: </label>
                    @if($setting && $setting->timekeeping_online == 1)
                        <a class="btn btn-success ml-2" href="{{route("timekeepingOff")}}"> Tắt</a>
                    @else
                        <a class="btn btn-success ml-2" href="{{route("timekeepingOn")}}"> Bật</a>
                    @endif
                </div>
            </div>

{{--            ngày nghỉ--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="card flex-fill">
                        <div class="card-body row">
                            <div class="col-md-12">
                                <div class="col-md-4 float-left"><h1>Ngày nghỉ</h1></div>
                                <div class="col-md-4 text-md-right float-right">
                                    <a href="#" class="btn btn-primary" data-toggle="modal"
                                       data-target="#create_day_off_modal"
                                    >Thêm</a>
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#import_file_attendance_modal">
                                        Import
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="import_file_attendance_modal" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Nhập ngày nghỉ hệ thống</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form name="import_attendance_form" id="import_attendance_form"
                                                          method="post" enctype="multipart/form-data"
                                                          action="{{route('import_day_off')}}">
                                                        @csrf
                                                        <div class="form-group row px-3">
                                                            <label for="import_file">Chọn file</label>
                                                            <input type="file"
                                                                   style="border: 1px solid #ced4da; border-radius: .25rem; width: 100%"
                                                                   name="import_file" required accept=".xlsx,.xls,.csv">
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer d-flex justify-content-end">
                                                    <button type="button" class="btn btn-outline-danger"
                                                            data-dismiss="modal">Hủy
                                                    </button>
                                                    <button type="submit" form="import_attendance_form"
                                                            class="btn btn-primary">Import
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                           <div class="col-md-12 row pl-5">
                               <div class="col-md-3 m10b">
                                   <form name="form_day_off_search" id="form_day_off_search" action="{{route('setting')}}"
                                         method="get">
                                       <input type="text" class="form-control" name="day_off_search"
                                              value="{{ request()->day_off_search }}"
                                              id="day_off_search" autocomplete="day_off_search"
                                              placeholder="Nội dung...">
                                   </form>
                               </div>
                               <div class="col-md-5 m10b">
                                   <button class="btn btn-success" id="button_leave_search" type="submit"
                                           form="form_day_off_search"
                                           aria-expanded="false">
                                       <i class="fa fa-search"> </i> Tìm kiếm
                                   </button>
                               </div>

                           </div>
                        </div>
                    </div>
                </div>
                {{--               ngày nghỉ table--}}
                <div class="col-md-12">
                    @if(!empty($day_offs))
                        {!! $day_offs->appends(request()->all())->links('layouts.paginator') !!}
                    @endif
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col" width="5%">#id</th>
                            <th scope="col" width="20%">Ngày nghỉ</th>
                            <th scope="col" width="55%">Chi tiết</th>
                            <th scope="col" width="20%">Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($day_offs as $item)
                            <tr>
                                <th scope="row">{{$item->id}}</th>
                                <td>{{$item->day_off}}</td>
                                <td>
                                    <div class="hiden-td-table">{{$item->content}}</div>
                                </td>
                                <td><a class="btn btn-warning btn_edit_day_off" data-toggle="modal"
                                       data-target="#edit_day_off_modal" data-data="{{$item}}">Sửa</a>
                                    <a class="btn btn-danger btn_confirmed_delete" data-id="{{$item->id}}"
                                       data-toggle="modal" data-target="#modalComfirmedDelete"
                                       href="{{route('dayOffDelete', $item->id)}}">Xóa</a></td>
                            </tr>
                        @empty
                            <tr>
                                <th scope="row" colspan="5" class="text-center"> Không có dữ liệu</th>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                        @if(!empty($day_offs))
                            {!! $day_offs->appends(request()->all())->links('layouts.paginator') !!}
                        @endif
                </div>
            </div>

            {{--            department--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="card flex-fill">
                        <div class="card-body row">
                            <div class="col-md-12">
                                <div class="col-md-4 float-left"><h1>Quản Phòng ban</h1></div>
                                <div class="col-md-4 text-md-right float-right">
                                    <a href="#" class="btn btn-primary" data-toggle="modal"
                                       data-target="#create_department_modal"
                                    >Thêm</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                {{--                department table--}}
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col" width="5%">#id</th>
                            <th scope="col" width="20%">Tên phòng ban</th>
                            <th scope="col" width="55%">Giới thiệu</th>
                            <th scope="col" width="20%">Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($list_department as $item)
                            <tr>
                                <th scope="row">{{$item->id}}</th>
                                <td>{{$item->department_name}}</td>
                                <td>
                                    <div class="hiden-td-table">{{$item->content}}</div>
                                </td>
                                <td><a class="btn btn-warning btn_edit_department" data-toggle="modal"
                                       data-target="#edit_department_modal" data-data="{{$item}}">Sửa</a>
                                    <a class="btn btn-danger btn_confirmed_delete" data-id="{{$item->id}}"
                                       data-toggle="modal" data-target="#modalComfirmedDelete"
                                       href="{{route('departmentDelete', $item->id)}}">Xóa</a></td>
                            </tr>
                        @empty
                            <tr>
                                <th scope="row" colspan="5" class="text-center"> Không có dữ liệu</th>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>

            {{--tax--}}
            <div class="row">
                {{--                table thue--}}
                <div class="col-md-12">
                    <div class="card flex-fill">
                        <div class="card-body row">
                            <div class="col-md-12">
                                <h1>Quản lý thuế</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#id</th>
                            <th scope="col">Từ (TNTT/tháng)</th>
                            <th scope="col">Tới (TNTT/tháng)</th>
                            <th scope="col">Phần trăm (%)</th>
                            <th scope="col">Mức trừ</th>
                            <th scope="col">Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list_tax as $tax)
                            <tr>
                                <th scope="row">{{$tax->id}}</th>
                                <td>{{$tax->taxation_start}}</td>
                                <td>{{$tax->taxation_end}}</td>
                                <td>{{$tax->percent}} (%)</td>
                                <td>{{$tax->tax_down}}</td>
                                <td><a class="btn btn-warning btn_edit_tax" data-toggle="modal"
                                       data-target="#edit_tax_modal" data-data="{{$tax}}">Sửa</a></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

    {{--modal eidt taxt--}}
    <div class="modal fade" id="edit_tax_modal" tabindex="-1" role="dialog" aria-labelledby="edit_tax_modalModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Sửa mức thuế</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="tax_update_modal" method="post" action="{{route('updateTax')}}">
                        @csrf
                        <input type="text" name="id" id="tax_id">
                        <div class="form-group row">
                            <label for="taxation_start" class="col-sm-3 col-form-label">Từ (TNTT/tháng)</label>
                            <div class="col-sm-9">
                                <input name="taxation_start" type="number" min="0"
                                       oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control"
                                       id="taxation_start" placeholder="số tiền">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="taxation_end" class="col-sm-3 col-form-label">tới (TNTT/tháng)</label>
                            <div class="col-sm-9">
                                <input name="taxation_end" type="number" min="1"
                                       oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control"
                                       id="taxation_end" placeholder="số tiền">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="percent" class="col-sm-3 col-form-label">Mức chịu thuế (%)</label>
                            <div class="col-sm-9">
                                <input name="percent" type="number" min="1" max="90"
                                       oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control"
                                       id="percent" placeholder="phần trăm >0 và <=90">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tax_down" class="col-sm-3 col-form-label">Giảm trừ</label>
                            <div class="col-sm-9">
                                <input name="tax_down" type="number" min="0"
                                       oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control"
                                       id="tax_down" placeholder="số tiền">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer d-flex justify-content-around">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary" form="tax_update_modal">Cập nhập</button>
                </div>
            </div>
        </div>
    </div>

    {{--    Modal create phòng ban--}}
    <div class="modal fade" id="create_department_modal" tabindex="-1" role="dialog"
         aria-labelledby="create_department_modal"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Thêm mới phòng ban</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="create_department_form" method="post" action="{{route('createDepartment')}}">
                        @csrf
                        <input type="text" name="id" id="create_department_id">
                        <div class="form-group row">
                            <label for="taxation_start" class="col-sm-3 col-form-label">Tên phòng ban</label>
                            <div class="col-sm-9">
                                <input name="department_name" required
                                       class="form-control"
                                       placeholder="Tên phòng ban">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="taxation_end" class="col-sm-3 col-form-label">Chi tiết</label>
                            <div class="col-sm-9">
                                <textarea name="content" class="form-control"  placeholder="Giới thiệu về phòng ban"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="taxation_end" class="col-sm-3 col-form-label">Trưởng phòng</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="head_id" name="head_id">
                                    <option></option>
                                    @foreach(getAllUser() as $item)
                                        <option value="{{$item->id}}">{{$item->username}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer d-flex justify-content-around">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary" form="create_department_form">Thêm</button>
                </div>
            </div>
        </div>
    </div>

{{--    modal edit phòng ban--}}
    <div class="modal fade" id="edit_department_modal" tabindex="-1" role="dialog"
         aria-labelledby="edit_department_modal"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Cập nhập phòng ban</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="edit_department_form" method="post" action="{{route('updateDepartment')}}">
                        @csrf
                        <input type="text" name="id" id="department_id" hidden>
                        <div class="form-group row">
                            <label for="department_name_edit" class="col-sm-3 col-form-label">Tên phòng ban</label>
                            <div class="col-sm-9">
                                <input name="department_name" required id="department_name_edit"
                                       class="form-control"
                                       placeholder="Tên phòng ban">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="content_edit" class="col-sm-3 col-form-label">Chi tiết</label>
                            <div class="col-sm-9">
                                <textarea name="content" id="content_edit" maxlength="253" class="form-control"  placeholder="Giới thiệu về phòng ban"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="taxation_end" class="col-sm-3 col-form-label">Trưởng phòng</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="head_id" name="head_id">
                                    <option></option>
                                    @foreach(getAllUser() as $item)
                                        <option value="{{$item->id}}">{{$item->username}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer d-flex justify-content-around">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary" form="edit_department_form">Cập nhập</button>
                </div>
            </div>
        </div>
    </div>

{{--    tạo ngày nghỉ--}}
    <div class="modal fade" id="create_day_off_modal" tabindex="-1" role="dialog"
         aria-labelledby="create_department_modal"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Thêm ngày nghỉ</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="create_day_off_modal_form" method="post" action="{{route('dayOffCreate')}}">
                        @csrf
                        <input type="text" name="id" id="create_department_id">
                        <div class="form-group row">
                            <label for="day_off" class="col-sm-3 col-form-label">Ngày</label>
                            <div class="col-sm-9">
                                <input name="day_off" required
                                       data-provide="datepicker"
                                       autocomplete="off"
                                       class="form-control day_off_date_timepicker"
                                       placeholder="ngày (năm-tháng-ngày)">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="content_day_off" class="col-sm-3 col-form-label">Chi tiết</label>
                            <div class="col-sm-9">
                                <textarea id="content_day_off" name="content" class="form-control"  placeholder="Chi tiết về ngày nghỉ"></textarea>
                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer d-flex justify-content-around">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary" form="create_day_off_modal_form">Thêm</button>
                </div>
            </div>
        </div>
    </div>

    {{--    sủa ngày nghỉ--}}
    <div class="modal fade" id="edit_day_off_modal" tabindex="-1" role="dialog"
         aria-labelledby="create_department_modal"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Sửa ngày nghỉ</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="edit_day_off_modal_form" method="post" action="{{route('dayOffUpdate')}}">
                        @csrf
                        <input type="text" name="id" id="day_off_id" hidden>
                        <div class="form-group row">
                            <label for="day_off_edit" class="col-sm-3 col-form-label">Ngày</label>
                            <div class="col-sm-9">
                                <input name="day_off" required
                                       id="day_off_edit"
                                       autocomplete="off"
                                       class="form-control"
                                       placeholder="ngày (năm-tháng-ngày)" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="content_day_off_edit" class="col-sm-3 col-form-label">Chi tiết</label>
                            <div class="col-sm-9">
                                <textarea id="content_day_off_edit" name="content" class="form-control"  placeholder="Chi tiết về ngày nghỉ"></textarea>
                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer d-flex justify-content-around">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary" form="edit_day_off_modal_form">Cập nhập</button>
                </div>
            </div>
        </div>
    </div>

@section('after_js')
    <script type="text/javascript">
        $(document).ready(function () {

            $("#percent").keydown(function () {
                // Save old value.
                if (!$(this).val() || (parseInt($(this).val()) <= 11 && parseInt($(this).val()) >= 0))
                    $(this).data("old", $(this).val());
            });
            $('#percent').on('input', function (e) {
                if (e.target.value > 0 && e.target.value <= 90) {
                    this.value = e.target.value
                } else {
                    this.value = $(this).data('old');
                }
                ;
            })

            $('.btn_edit_day_off').click(function (e) {
                let data = $(this).data('data');
                $('#day_off_id').val(data.id ? data.id : '');
                $('#day_off_edit').val(data.day_off ? data.day_off : '');
                $('#content_day_off_edit').val(data.content ? data.content : '');
            })

            //depratment
            $('.btn_edit_department').click(function (e) {
                let data = $(this).data('data');
                $('#department_id').val(data.id ? data.id : '');
                $('#department_name_edit').val(data.department_name ? data.department_name : '');
                $('#content_edit').val(data.content ? data.content : '');
            })

            //    edit tax
            $('.btn_edit_tax').click(function (e) {
                let data = $(this).data('data');
                $('#tax_id').val(data.id ? data.id : '');
                $('#taxation_start').val(data.taxation_start ? data.taxation_start : '');
                $('#taxation_end').val(data.taxation_end ? data.taxation_end : '');
                $('#percent').val(data.percent ? data.percent : '');
                $('#tax_down').val(data.tax_down ? data.tax_down : '');
            })

        });
    </script>
@endsection
@endsection
