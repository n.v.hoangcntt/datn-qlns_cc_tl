@extends('layouts.layout_admin')

@section('title')
    {{trans('layout.Dashboard')}}
@endsection


@section('title_page')
    {{trans('layout.Home Manager')}}
@endsection
@section('content')
    <div class="statistical">
        <div class="stat_box">
            <h4>Số người dùng</h4>
            <div class="stat_con">
                <h2>{{$countUser}}</h2>
{{--                <div class="stat stat_up">200%</div>--}}
            </div>

        </div>
{{--        <div class="stat_box">--}}
{{--            <h4>Transactions</h4>--}}
{{--            <div class="stat_con">--}}
{{--                <h2>5,210</h2>--}}
{{--            </div>--}}

{{--        </div>--}}


    </div>
    <div class="row card-group">
        <div class="col-12 col-sm-12 col-md-12 d-flex">
            <div class="card flex-fill">
                <div class="card-header">
                    <h3>Chức năng thực hiện</h3>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{route('leave_attendance')}}">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <i class="iconsminds-coins" style="font-size: 100px;" ></i>
                                        <h3>{{__('layout.Leave & Attendance Management')}}</h3>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-sm-6">
                            <a href="{{route('user_management.index')}}">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <i class="iconsminds-male-female" style="font-size: 100px;"></i>
                                        <h3>{{__('layout.User Management')}}</h3>
                                    </div>
                                </div>
                            </a>

                        </div>

                        <div class="col-sm-6">
                            <a href="{{route('indexReport')}}">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <i class="iconsminds-pie-chart" style="font-size: 100px;"></i>
                                        <h3>{{__('layout.Report')}}</h3>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-sm-6">
                            <a href="{{route('setting')}}">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <i class="iconsminds-gears" style="font-size: 100px;"></i>
                                        <h3>{{__('layout.Setting')}}</h3>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <ul class="">


                        <li></li>
                    <li class="{{activeRoute(URI_SETTING)}}"></li>
                </ul>
            </div>
        </div>
    </div>
    <!--end card-->

    </div>

@endsection
