@extends('layouts.layout_admin')

@section('title')
    {{trans('layout.Report')}}
@endsection


@section('title_page')
    {{trans('layout.Report')}}
@endsection
@section('section_header')
    {{--    <a href="#" class="btn btn-primary btn-add" data-toggle="modal" data-target="#"><i--}}
    {{--            class="iconsminds-add-user"></i> Add user</a>--}}
@endsection
@section('before_js')
    <script type="text/javascript"
            src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js" defer></script>

@endsection
@section('content')

    {{--tạo báo cáo--}}
    <div class="filter-wrapper">
        <div class="card flex-fill">
            <div class="card-body row">
                {{--                tạo báo cáo--}}
                <div class="col-md-12 row">
                    <div class="col-md-12">
                        <h3>Tạo báo cáo</h3>
                    </div>
                    <div class="col-md-12">
                        <form name="create_report" id="create_report" action="{{route('createReports')}}" method="post">
                            @csrf
                            <div class="form-group form-inline col-md-8 float-left">
                                <select class="form-control mr-2" id="type_report" name="type_report"
                                        onchange="typeReportOnchange(this)"
                                        required>
                                    <option></option>
                                    <option value="user">Nhân viên</option>
                                    <option value="luongThangTruoc">Lương tháng trước</option>
                                    {{--                                    <option value="attendance">Chấm công tháng trước</option>--}}
                                </select>

                                <select class="form-control mr-2" id="phongban" name="phongban" style="display: none">
                                    <option value="">Tất cả</option>
                                    @foreach(getAllDepartment() as $item)
                                        <option value="{{$item->id}}">{{$item->department_name}}</option>
                                    @endforeach


                                </select>
                                <input type="text" class="form-control" id="content" name="content"
                                       placeholder="Tiêu đề"
                                       autocomplete="off" required>
                            </div>

                            <button type="submit" class="col-md-2 btn btn-primary mb-2  float-right">Tạo</button>
                        </form>
                    </div>
                    {{--                    @if (\Carbon\Carbon::now()->day < 5)--}}
                    {{--                     <a href="{{route('tinhLuong')}}" class="btn btn-primary btn-sm"> Tính lương</a>
                    {{--                    @endif--}}
                </div>
                <div class="col-md-12 row pt-3 pl-5">
                    <div class="col-md-5 m10b">
                        <form name="form_search_report" id="form_search_report" action="{{route('indexReport')}}"
                              method="get">
                            <input type="text" class="form-control" name="key_search_report"
                                   value="{{request()->key_search_report}}"
                                   id="key_search_report" autocomplete="key_search_report" placeholder="Tiêu đề">
                        </form>
                    </div>
                    <div class="col-md-5 m10b">
                        <button class="btn btn-success" type="submit"
                                form="form_search_report"
                                aria-expanded="false">
                            <i class="fa fa-search"> </i> Tìm kiếm
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="table-responsive table-hover table-user">
                @if(!empty($reports))
                    {!! $reports->appends(Arr::except(request()->all(), ['salary', 'key_search_salary']))->links('layouts.paginator') !!}
                @endif
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col" style="width: 20px">#ID</th>
                        <th class="text-center" scope="col">Thao tác</th>
                        <th scope="col" style="width: 50%">Tiêu đề</th>
                        <th scope="col">Ngày tạo</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($reports as $item)
                        <tr>
                            <th scope="row" style="width: 20px">{{$item->id}}</th>
                            <td scope="col" style="min-width: 198px">
                                <a class="btn btn-danger btn_confirmed_delete" data-id="{{$item->id}}"
                                   data-toggle="modal" data-target="#modalComfirmedDelete"
                                   href="{{route('delete_report', $item->id)}}">Xóa</a>
                                <a class="btn btn-primary"
                                   href="{{$item->link}}" download>Tải về </a>
                                <a class="btn btn-success btn_view_file" data-id="{{$item->id}}"
                                   data-link="{{$item->link_pdf}}"
                                   href="">Xem</a>
                            </td>
                            <td>{{$item->content}}</td>
                            <td>{{$item->created_at}}</td>

                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" class="text-center text-secondary">Không có dữ liệu!</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
                @if(!empty($reports))
                    {!! $reports->appends(Arr::except(request()->all(), ['salary', 'key_search_salary']))->links('layouts.paginator') !!}
                @endif
            </div>
        </div>
    </div>
    <!--end card-->


    {{--    tinh lương--}}
    <div class="filter-wrapper">
        <div class="card flex-fill">
            <div class="card-body row">
                {{--                bảng luong--}}
                <div class="col-md-12 row">
                    <div class="col-md-12">
                        <h3> Quản lý tính lương:</h3>
                        <a href="{{route('tinhLuong')}}" class="btn btn-primary btn-sm"> Tính lương</a>
                    </div>
                    <div class="col-md-12">

                    </div>
                    {{--                    @if (\Carbon\Carbon::now()->day < 5)--}}
                    {{--                     <a href="{{route('tinhLuong')}}" class="btn btn-primary btn-sm"> Tính lương</a>
                    {{--                    @endif--}}
                </div>
                <div class="col-md-12 row pt-5">
                    <div class="col-md-5 m10b">
                        <form name="form_search_report" id="form_search_salary" action="{{route('indexReport')}}"
                              method="get">
                            <input type="text" class="form-control" name="key_search_salary"
                                   value="{{request()->key_search_salary}}"
                                   id="key_search_salary" autocomplete="key_search_salary"
                                   placeholder="username/tháng">
                        </form>
                    </div>
                    <div class="col-md-5 m10b">
                        <button class="btn btn-success" type="submit"
                                form="form_search_salary"
                                aria-expanded="false">
                            <i class="fa fa-search"> </i> Tìm kiếm
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="table-responsive table-hover table-user">
                @if(!empty($salary))
                    {!! $salary->appends(Arr::except(request()->all(), ['reports', 'key_search_report']))->links('layouts.paginator') !!}
                @endif
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col" style="min-width: 10px">#ID</th>
                        <th class="text-center" scope="col" style="width: 50px">Thao tác</th>
                        <th scope="col">UserName</th>
                        <th scope="col" class="text-center">Lương nhận</th>
                        <th scope="col" class="text-center">Tổng lương</th>
                        <th scope="col" class="text-center">Năm-tháng</th>
                        <th scope="col" class="text-center">Số công</th>
                        <th class="text-center" scope="col">Trợ cấp</th>
                        <th scope="col">Phí bảo hiểm</th>
                        <th class="text-center" scope="col">Phí khác</th>
                        <th class="text-center" scope="col">Thưởng</th>
                        <th class="text-center" scope="col">Phạt</th>
                        <th class="text-center" scope="col">Tổng phút muộn</th>
                        <th class="text-center" scope="col">Trung bình muộn</th>
                        <th class="text-center" scope="col">Tăng Ca</th>
                        <th class="text-center" scope="col">Trạng thái</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($salary as $item)
                        <tr>
                            <th scope="row" style="width: 28px">{{$item->id}}</th>
                            <td scope="col" style="min-width: 138px">
                                <a class="btn btn-warning btn_edit_salary"
                                   data-target="#edit_salary"
                                   data-data="{{$item}}">Sửa</a>
                                <a class="btn btn-danger btn_confirmed_delete" data-id="{{$item->id}}"
                                   data-toggle="modal" data-target="#modalComfirmedDelete"
                                   href="{{route('delete_salary', $item->id)}}">Xóa</a>
                            </td>
                            <th scope="col">{{$item->user['username']}}</th>
                            <th scope="col" class="text-center" style="min-width: 158px">{{number_format($item->result_salary)}} vnđ</th>
                            <th scope="col" class="text-center"  style="min-width: 158px">{{number_format($item->total_salary)}} vnđ</th>
                            <th scope="col" class="text-center" style="min-width: 130px">{{$item->month_of_year}}</th>
                            <th scope="col" class="text-center"  style="min-width: 100px">{{$item->total_workday}}</th>
                            <th class="text-center" scope="col" style="min-width: 158px">{{number_format($item->allowance)}} vnđ</th>
                            <th scope="col" style="min-width: 158px">{{number_format($item->social_security)}} vnđ</th>
                            <th class="text-center" scope="col" style="min-width: 158px">{{number_format($item->other)}} vnđ</th>
                            <th class="text-center" scope="col" style="min-width: 158px">{{number_format($item->bonus)}} vnđ</th>
                            <th class="text-center" scope="col" style="min-width: 158px">{{number_format($item->mulct)}} vnđ</th>
                            <th class="text-center" scope="col" style="min-width: 158px">{{$item->total_late_time}}</th>
                            <th class="text-center" scope="col" style="min-width: 158px">{{$item->average_late_time}}</th>
                            <th class="text-center" scope="col" style="min-width: 158px">{{$item->total_over_time}}</th>
                            <th class="text-center" scope="col" style="min-width: 158px"> {{$item->status}}</th>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="16" class="text-center text-secondary">Không có dữ liệu!</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
                @if(!empty($salary))
                    {!! $salary->appends(Arr::except(request()->all(), ['reports', 'key_search_report']))->links('layouts.paginator') !!}
                @endif
            </div>
        </div>
    </div>
    <!--end card-->

    {{--modal sửa bảng lương--}}
    <div class="modal fade" id="edit_salary" tabindex="-1" role="dialog"
         aria-labelledby="edit_salary"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Sửa bảng lương</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="edit_salary_form" method="post" action="{{route('update_salary')}}">
                        @csrf
                        <input type="text" name="id" id="salary_id" >
                        <input type="text" name="user_id" id="user_id_salary" >

                        <div class="form-group row">
                            <label for="username_salary" class="col-sm-3 col-form-label">Username</label>
                            <div class="col-sm-9">
                                <input
                                    id="username_salary"
                                    readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="month_of_year_salary" class="col-sm-3 col-form-label">LƯơng tháng</label>
                            <div class="col-sm-9">
                                <input name="month_of_year"
                                       id="month_of_year_salary"
                                       readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="allowance_salary" class="col-sm-3 col-form-label">Trợ cấp</label>
                            <div class="col-sm-9">
                                <input name="allowance"
                                       class="form-control"
                                       id="allowance_salary"
                                       type="number">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="bonus_salary" class="col-sm-3 col-form-label">Thưởng</label>
                            <div class="col-sm-9">
                                <input name="bonus"
                                       class="form-control"
                                       id="bonus_salary"
                                       type="number"
                                >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mulct_salary" class="col-sm-3 col-form-label">Phạt</label>
                            <div class="col-sm-9">
                                <input name="mulct"
                                       class="form-control"
                                       type="number"
                                       id="mulct_salary"
                                >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="other_salary" class="col-sm-3 col-form-label">Phí khác</label>
                            <div class="col-sm-9">
                                <input name="other"
                                       class="form-control"
                                       type="number"
                                       id="other_salary"
                                >
                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer d-flex justify-content-around">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary" form="edit_salary_form">Cập nhập</button>
                </div>
            </div>
        </div>
    </div>

@section('after_js')
    <script type="text/javascript">
        function typeReportOnchange(data) {
            const  now = new Date();

            if (data.value == 'user') {
                $('#phongban').css('display', 'block');
            } else {
                $('#phongban').css('display', 'none');
            }
            if (data.value == 'luongThangTruoc') {
                $('#content').val('Lương tháng ' + now.getMonth() + ' năm ' +now.getFullYear())
            }else{
                $('#content').val('');
            }
        }

        $(document).ready(function () {

            $('.btn_edit_salary').click(function (e) {
                e.preventDefault();
                let data = $(this).data('data');
                $('#salary_id').val(data.id);
                $('#user_id_salary').val(data.user_id);
                $('#username_salary').val(data.user.username);
                $('#month_of_year_salary').val(data.month_of_year);
                $('#allowance_salary').val(data.allowance);
                $('#bonus_salary').val(data.bonus);
                $('#mulct_salary').val(data.mulct);
                $('#other_salary').val(data.other);
                $('#edit_salary').modal('show');
            })

        });
    </script>
@endsection

@endsection
