<?php

return [
    'Home Manager' => 'TRANG CHỦ QUẢN LÝ',
    'User Management' => 'QUẢN LÝ NHÂN SỰ',
    'Leave & Attendance Management' => 'QUẢN LÝ CÔNG, NGHỈ',
    'Dashboard' => 'TRANG CHỦ',
    'Setting' => 'CÀI ĐẶT',
    'Report' => 'BÁO CÁO THỐNG KÊ',
];
