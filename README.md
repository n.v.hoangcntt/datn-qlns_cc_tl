<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# Clone project 
cd datn-qlns_cc_tl

run:   <code> git clone git@gitlab.com:n.v.hoangcntt/datn-qlns_cc_tl.git </code>

Run 

windown 
~~~~
copy env-example .env
php artisan key:generate
~~~~

linux or macOS 
~~~~
cp env-example .env
php artisan key:generate
~~~~

if use windown run: <code> git config core.autocrlf true </code> để bỏ theo dõi việc git chuyển LF sang CRLF

    composer install
    php artisan vendor:publish --provider="Maatwebsite\Excel\ExcelServiceProvider"
#### Package

- bootstrap-4.3.1
- font-awesome
- bootstrap-datepicker https://bootstrap-datepicker.readthedocs.io/en/latest/
https://xdsoft.net/jqplugins/datetimepicker/

- date format js : https://github.com/phstc/jquery-dateFormat
